#include "settings_model.hh"

SettingsModel::SettingsModel(QObject *parent)
  : QObject(parent)
{
    for (int i = 0; i < SettingsSections::Count; ++i)
    {
        items_.push_back(nullptr);
    }

    items_[SettingsSections::General] = new SettingsSection("General",
        this,
        new SettingsEntry{ "server_settings", "Connection Settings", this },
        new SettingsEntry{ "refresh_interval", "Refresh Interval", this });
}

SettingsModel::~SettingsModel() = default;
