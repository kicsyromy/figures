import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

import "components" as Components

ListView {
    id: listView
    model: ServerDataModel.cpuStatistics.data

    readonly property int textSize: 14

    boundsMovement: Flickable.StopAtBounds
    boundsBehavior: Flickable.DragOverBounds

    clip: true

    ScrollIndicator.vertical: ScrollIndicator { }

    delegate: ItemDelegate {
        width: listView.width
        height: 130

        highlighted: ListView.isCurrentItem
        onClicked: listView.currentIndex = index

        horizontalPadding: 20

        ColumnLayout {
            id: delegateRoot
            x: parent.horizontalPadding
            width: parent.width - x - parent.horizontalPadding

            spacing: 10

            anchors {
                verticalCenter: parent.verticalCenter
            }

            RowLayout {
                spacing: 10

                Label {
                    Layout.fillWidth: true

                    text: modelData.processor
                    font.pointSize: textSize
                    elide: Text.ElideRight
                }

                Label {
                    text: modelData.frequency + " MHz"
                    font.pointSize: textSize
                    elide: Text.ElideRight
                }
            }

            Components.UsageBar {
                height: 30
                Layout.fillWidth: true

                usage: modelData.activePercent / 100.0
            }

            RowLayout {
                Label {
                    Layout.fillWidth: true

                    text: modelData.temperatureCurrent + "°C"
                    font.pointSize: textSize
                    elide: Text.ElideRight
                }

                Label {
                    text: modelData.activePercent + "%"
                    font.pointSize: textSize
                    elide: Text.ElideRight
                }
            }
        }
    }
}
