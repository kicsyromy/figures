import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

import "components" as Components
import "components/helpers.js" as Js

ListView {
    id: root
    model: [
        [ qsTr("System Memory"), ServerDataModel.memoryStatistics.bytesTotal, ServerDataModel.memoryStatistics.bytesUsed ],
        [ qsTr("SWAP Space"), ServerDataModel.memoryStatistics.swapBytesTotal, ServerDataModel.memoryStatistics.swapBytesUsed ],
    ]

    readonly property int textSize: 14

    boundsMovement: Flickable.StopAtBounds
    boundsBehavior: Flickable.DragOverBounds

    clip: true

    ScrollIndicator.vertical: ScrollIndicator { }

    delegate: ItemDelegate {
        width: root.width
        height: 110

        highlighted: ListView.isCurrentItem
        onClicked: root.currentIndex = index

        horizontalPadding: 20

        ColumnLayout {
            id: delegateRoot
            x: parent.horizontalPadding
            width: parent.width - x - parent.horizontalPadding

            spacing: 10

            anchors {
                verticalCenter: parent.verticalCenter
            }

            RowLayout {
                spacing: 10

                Label {
                    Layout.fillWidth: true

                    text: modelData[0]
                    font.pointSize: textSize
                    elide: Text.ElideRight
                }

                Label {
                    text: Js.humanReadable(modelData[1])
                    font.pointSize: textSize
                    elide: Text.ElideRight
                }
            }

            Components.UsageBar {
                height: 30
                Layout.fillWidth: true

                usage: modelData[2] / modelData[1]
            }

            Label {
                Layout.fillWidth: true

                text: Js.humanReadable(modelData[2])
                font.pointSize: textSize

                elide: Text.ElideRight
                horizontalAlignment: Text.AlignRight
            }
        }
    }
}
