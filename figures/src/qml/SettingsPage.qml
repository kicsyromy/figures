import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

import Figures 1.0
import "components" as Components

ListView {
    id: root

    ScrollIndicator.vertical: ScrollIndicator { }

    boundsMovement: Flickable.StopAtBounds
    boundsBehavior: Flickable.DragOverBounds

    model: SettingsModel.items

    delegate: Item {
        width: root.width
        height: childrenRect.height + 5

        property int currentSection: index

        Pane {
            id: pane

            width: parent.width
            padding: 0
            topPadding: 8

            Material.elevation: 6

            ColumnLayout {
                width: parent.width
                spacing: 5

                Label {
                    Layout.leftMargin: 15
                    text: modelData.name
                    color: "#a5d6a7"
                }

                Column {
                    spacing: 0

                    Repeater {
                        id: repeater

                        model: modelData.entries

                        ItemDelegate {
                            width: pane.width

                            RowLayout {
                                x: 15
                                anchors.verticalCenter: parent.verticalCenter

                                spacing: 25

                                Components.SvgIcon {
                                    source: modelData.icon
                                }

                                Label {
                                    text: modelData.name
                                }
                            }

                            onClicked: {
                                priv.dialogs[index].open()
                            }
                        }
                    }
                }
            }
        }
    }

    ConnectionSettingsDialog {
        id: connectionsSettings

        width: parent.width / 1.2

        x: parent.width / 2 - width / 2
        y: 50
    }

    RefreshIntervalDialog {
        id: refreshIntervalSettings

        width: parent.width / 1.2

        x: parent.width / 2 - width / 2
        y: 50
    }

    QtObject {
        id: priv

        property var dialogs: [
            connectionsSettings,
            refreshIntervalSettings
        ]
    }
}
