import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

import Figures 1.0
import "components" as Components
import "components/helpers.js" as Js

ListView {
    id: listView

    property QtObject selectedDrive: null

    Label {
        id: dummy
        width: 0
        height: 0
        visible: false
    }

    readonly property int horizontalPadding: 20
    readonly property int textSize: 14

    onVisibleChanged: {
        if (!visible) {
            selectedDrive = null
        }
    }

    header:  Pane {
        id: header

        visible: selectedDrive !== null && selectedDrive.isPhysicalDrive

        width: listView.width
        height: selectedDrive !== null && selectedDrive.isPhysicalDrive ?
                    (selectedDrive.hasSmartInfo ? 180 : 130) :
                    0

        leftPadding: listView.horizontalPadding
        rightPadding: listView.horizontalPadding
        topPadding: 8

        Material.elevation: 6

        property int maximumTextLength: 0

        property var model: selectedDrive !== null ? [
            [ qsTr("Device File:"), selectedDrive.blockDevices.deviceFile],
            [ qsTr("Serial Number:"), selectedDrive.serialNumber],
            [ qsTr("Capacity:"), Js.humanReadable(selectedDrive.capacity)],
            [ qsTr("Temperature:"), selectedDrive.temperature + " °C" ],
            [ qsTr("SMART Status:"), selectedDrive.smartStatus ],
        ] : null

        ColumnLayout {
            spacing: 10

            anchors {
                verticalCenter: parent.verticalCenter
            }

            Repeater {
                model: header.model

                RowLayout {
                    Layout.fillWidth: true
                    visible: index < 3 || selectedDrive.hasSmartInfo

                    Label {
                        Layout.minimumWidth: header.maximumTextLength
                        Layout.maximumWidth: header.maximumTextLength

                        onImplicitWidthChanged: {
                            if (implicitWidth + 10 > header.maximumTextLength) {
                                header.maximumTextLength = implicitWidth + 10
                            }
                        }

                        text: modelData[0]
                        elide: Text.ElideRight
                        font.pointSize: listView.textSize
                    }


                    Label {
                        id: label

                        text: modelData[1]
                        elide: Text.ElideRight
                        font.pointSize: listView.textSize
                    }
                }
            }
        }

    }

    model: selectedDrive !== null ? selectedDrive.blockDevices : null

    boundsMovement: Flickable.StopAtBounds
    boundsBehavior: Flickable.DragOverBounds

    clip: true

    ScrollIndicator.vertical: ScrollIndicator { }

    delegate: ItemDelegate {
        width: listView.width
        height: 80

        highlighted: ListView.isCurrentItem
        onClicked: {
            listView.currentIndex = index
        }

        horizontalPadding: listView.horizontalPadding

        ColumnLayout {
            id: delegateRoot
            x: parent.horizontalPadding
            width: parent.width - x - parent.horizontalPadding

            spacing: 10

            anchors {
                verticalCenter: parent.verticalCenter
            }

            RowLayout {
                spacing: 10

                Label {
                    Layout.fillWidth: true

                    text: blockDevice.deviceFile
                    elide: Text.ElideRight
                    font.pointSize: listView.textSize
                }

                Label {
                    text: Js.humanReadable(blockDevice.sizeBytes)
                    elide: Text.ElideRight
                    font.pointSize: listView.textSize
                }
            }

            RowLayout {
                spacing: 10

                Label {
                    Layout.fillWidth: true

                    readonly property bool isMounted: blockDevice.filesystem.mountPoints.length > 0

                    text: blockDevice.filesystem.mountPoints.toString()
                    elide: Text.ElideRight
                    font.pointSize: listView.textSize
                }

                Label {
                    text: blockDevice.filesystem.filesystem
                    elide: Text.ElideRight
                    font.pointSize: listView.textSize
                }
            }
        }
    }
}
