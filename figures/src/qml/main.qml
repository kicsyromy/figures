import QtQml 2.12
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

import "components" as Components

ApplicationWindow {
    id: mainWindow
    width: 640
    height: 1000
    visible: true
    title: qsTr("Figures")

    Material.theme: Material.Dark
    Material.accent: Material.Green
    Material.primary: Material.color(Material.Grey, Material.Shade900)

    Shortcut {
        sequence: "Menu"
        onActivated: optionsMenuAction.trigger()
    }

    Action {
        id: optionsMenuAction
        onTriggered: optionsMenu.open()
    }

    Action {
        id: backButtonAction
        onTriggered: stackView.pop()
    }

    Shortcut {
        sequences: ["Esc", "Back"]
        enabled: stackView.depth > 1
        onActivated: backButtonAction.trigger()
    }

    property TabBar tabBar
    property bool extraPagesOpen: stackView.depth > 1

    header: ToolBar {
        RowLayout {
            anchors.fill: parent

            ToolButton {
                id: backButton
                visible: extraPagesOpen

                action: backButtonAction

                Components.SvgIcon {
                    anchors.centerIn: parent
                    source: "arrow_back"
                }
            }

            Label {
                id: title
                visible: extraPagesOpen

                Layout.fillWidth: true

                text: "" + stackView.currentItem.title
                font.pointSize: 16
                font.bold: true

            }

            TabBar {
                id: bar
                clip: true
                visible: !extraPagesOpen
                currentIndex: swipeView.currentIndex

                Layout.fillWidth: true
                Material.background: Material.color(Material.Grey, Material.Shade900)

                Repeater {
                    model: [
                        qsTr("Overview"),
                        qsTr("CPU"),
                        qsTr("Memory"),
                        qsTr("Storage"),
                        qsTr("Processes")
                    ]

                    TabButton {
                        text: modelData
                        width: Math.max(implicitWidth, tabBar.width / 5)
                    }
                }

                // FIXME: Tried to bind this thing in every which-way
                //        nothing worked so resorted to JavaScript 🤮
                onCurrentIndexChanged: {
                    ServerDataModel.memoryStatistics.refreshing = false
                    ServerDataModel.cpuStatistics.refreshing = false
                    ServerDataModel.diskInfo.refreshing = false
                    ServerDataModel.processList.refreshing = false

                    switch (currentIndex)
                    {
                    case 0:
                        break;
                    case 1:
                        ServerDataModel.cpuStatistics.refreshing = true
                        break
                    case 2:
                        ServerDataModel.memoryStatistics.refreshing = true
                        break
                    case 3:
                        ServerDataModel.diskInfo.refreshing = true
                        break
                    case 4:
                        ServerDataModel.processList.refreshing = true
                        break
                    }
                }

                Component.onCompleted: {
                    tabBar = bar
                }
            }

            ToolButton {
                action: optionsMenuAction

                visible: !extraPagesOpen

                Components.SvgIcon {
                    anchors.centerIn: parent
                    source: "more_vert"
                }

                Menu {
                    id: optionsMenu
                    x: parent.width - width
                    transformOrigin: Menu.TopRight

                    Action {
                        text: "Settings"
                        onTriggered: stackView.push(settingsPage)
                    }
                }
            }
        }
    }

    StackView {
        id: stackView
        anchors.fill: parent

        initialItem: SwipeView {
            id: swipeView
            currentIndex: tabBar.currentIndex

            Item {
                id: overview
                Label {
                    text: "hello 1"
                }
            }

            CpuStats {
                id: cpu
            }

            MemoryStats {
                id: memory
            }

            DiskInfo {
                id: storage
                stackView: stackView
            }

            ProcessList {
                id: processList
                inView: tabBar.currentIndex === 4 && stackView.depth === 1
            }
        }
    }

    Component {
        id: settingsPage

        SettingsPage {
            property string title: qsTr("Settings")
        }
    }

    Connections {
        target: ServerDataModel
        function onErrorOccured(message) {
            errorDialog.message = message
            errorDialog.open()
        }
    }

    ErrorDialog {
        id: errorDialog
        x: (mainWindow.width - width) / 2
        y: mainWindow.height / 6
        width: Math.min(mainWindow.width, mainWindow.height) / 3 * 2

        message: ""
    }
}
