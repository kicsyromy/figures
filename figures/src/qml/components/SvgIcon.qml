import QtQuick 2.12
import QtQuick.Window 2.12

// Hack to get SVG to render without being blurry on HiDPI screens
Item {
    id: root

    width: 24
    height: 24

    property string source

    Image {
        id: image
        anchors.fill: parent

        sourceSize.width: 128
        sourceSize.height: 128

        source: "../icons/" + root.source + "_" + ApplicationSettings.theme
    }
}
