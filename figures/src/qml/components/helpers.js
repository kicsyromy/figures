function humanReadable(bytes) {
    let factor = 1000.0
    let byteSuffix = factor - 1000.0 < 0.001 ? qsTr("B") : qsTr("iB")

    if (bytes < 0) return "" + bytes + " " + byteSuffix

    var result = ""
    var newValue = bytes

    var i = 0;
    for (; newValue - 1 > 0.001; ++i) {
        newValue /= factor
    }
    result = "" + (newValue * factor).toFixed(2)
    // Went one to far, so roll back the iteration value by one
    --i;

    switch (i) {
    default:
        result += " " + byteSuffix
        break;
    case 1:
        result += " K" + byteSuffix
        break
    case 2:
        result += " M" + byteSuffix
        break
    case 3:
        result += " G" + byteSuffix
        break
    case 4:
        result += " T" + byteSuffix
        break
    }

    return result
}
