import QtQuick 2.12

Item {
    id: root

    property real usage: 1.0

    Rectangle {
        anchors.fill: parent
        color: Qt.rgba(0, 0, 0, 0.1)
        border {
            color: "darkgray"
        }
        radius: 2
    }

    Rectangle {
        id: currentProgress
        width: parent.width * root.usage
        height: parent.height
        color: Qt.rgba(1, 1, 1, 0.9)
        radius: 2
    }
}
