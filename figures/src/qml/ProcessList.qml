import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

import "components" as Components
import "components/helpers.js" as Js

Item {
    id: root

    Label {
        id: dummy
        width: 0
        height: 0
        visible: false
    }

    property bool inView: false

    readonly property int textSize: dummy.font.pointSize
    readonly property int columnPidWidth: 6 * textSize
    readonly property int columnUidWidth: 10 * textSize
    readonly property int columnRssWidth: 8 * textSize
    readonly property int horizontalPadding: 20
    readonly property int columnSpacing: 20

    Shortcut {
        sequences: ["Esc", "Back"]
        enabled: inView && !filterTextField.hidden
        onActivated: filterTextField.hide()
    }

    TextField {
        id: filterTextField

        readonly property bool hidden: y < 0

        anchors {
            left: parent.left
            leftMargin: horizontalPadding
            right: parent.right
            rightMargin: horizontalPadding
        }

        height: implicitHeight

        onTextChanged: {
            ServerDataModel.processList.modelData.setFilterRegularExpression(text)
        }

        Behavior on y {
            YAnimator {
                from: -implicitHeight
                to: 0
                duration: 100
            }
        }

        function show() {
            y = 0
            focus = true
        }

        Behavior on y {
            YAnimator {
                from: 0
                to: -implicitHeight
                duration: 100
            }
        }

        function hide() {
            y = -implicitHeight
            text = ""
        }

        Component.onCompleted: {
            hide()
        }
    }

    ListView {
        id: listView

        anchors {
            top: filterTextField.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        spacing: 0
        clip: true

        model: ServerDataModel.processList.modelData

        boundsMovement: Flickable.StopAtBounds
        boundsBehavior: Flickable.DragOverBounds

        ScrollIndicator.vertical: ScrollIndicator { }

        header: Item {
        }

        headerPositioning: ListView.PullBackHeader

        delegate: ItemDelegate {
            width: listView.width
            height: 115

            highlighted: ListView.isCurrentItem
            onClicked: {
                listView.currentIndex = index
                processInfoDialog.process = processInfo
                processInfoDialog.open()
            }

            horizontalPadding: horizontalPadding

            ColumnLayout {
                id: delegatelistView

                x: parent.horizontalPadding
                width: parent.width - x - parent.horizontalPadding
                anchors.verticalCenter: parent.verticalCenter

                Label {
                    Layout.fillWidth: true

                    text: processInfo.commandLine !== "" ? processInfo.commandLine : processInfo.name
                    elide: Text.ElideRight
                    font.bold: true
                    font.pointSize: 14
                }

                RowLayout {
                    id: usageLayout
                    spacing: 0
                    Layout.fillWidth: true

                    property int usageColumnWidth: 5 * textSize

                    Label {
                        Layout.minimumWidth: usageLayout.usageColumnWidth
                        Layout.maximumWidth: usageLayout.usageColumnWidth

                        text: "CPU:"
                        elide: Text.ElideRight
                    }

                    Components.UsageBar {
                        height: textSize
                        Layout.fillWidth: true

                        usage: processInfo.cpuUsage / (ServerDataModel.processList.coreCount * 100.0)
                    }

                    Label {
                        Layout.minimumWidth: usageLayout.usageColumnWidth
                        Layout.maximumWidth: usageLayout.usageColumnWidth
                        text: processInfo.cpuUsage.toFixed(2) + " %"
                        elide: Text.ElideRight
                        horizontalAlignment: Text.AlignRight

                    }

                }

                RowLayout {
                    spacing: 0
                    Layout.fillWidth: true

                    Label {
                        Layout.minimumWidth: usageLayout.usageColumnWidth
                        Layout.maximumWidth: usageLayout.usageColumnWidth
                        text: "Memory:"
                        elide: Text.ElideRight
                    }

                    Components.UsageBar {
                        height: textSize
                        Layout.fillWidth: true

                        usage: processInfo.bytesRss / ServerDataModel.processList.totalMemory
                    }

                    Label {
                        Layout.minimumWidth: usageLayout.usageColumnWidth
                        Layout.maximumWidth: usageLayout.usageColumnWidth
                        text: Js.humanReadable(processInfo.bytesRss)
                        elide: Text.ElideRight
                        horizontalAlignment: Text.AlignRight
                    }
                }
            }
        }

        ProcessInfoDialog {
            id: processInfoDialog

            width: parent.width / 1.2

            x: parent.width / 2 - width / 2
            y: 50
        }
    }


    RoundButton {
        width: 64
        height: 64

        anchors {
            bottom: parent.bottom
            bottomMargin: horizontalPadding * 2
            right: parent.right
            rightMargin: horizontalPadding * 2
        }

        Components.SvgIcon {
            anchors.centerIn: parent

            source: "filter_list"
        }

        onClicked: {
            filterTextField.hidden ? filterTextField.show() : filterTextField.hide()
        }
    }
}
