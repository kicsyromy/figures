import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Dialog {
    id: root
    modal: true
    focus: true
    title: qsTr("Connection Settings")
    contentHeight: column.height

    ColumnLayout {
        id: column
        width: parent.width
        spacing: 10

        Label {
            Layout.fillWidth: true
            elide: Text.ElideRight
            text: qsTr("Address")
        }

        TextField {
            id: serverUrl
            Layout.fillWidth: true
            focus: true
            text: ApplicationSettings.serverUrl
        }

        Label {
            Layout.fillWidth: true
            elide: Text.ElideRight
            text: qsTr("Authentication Token")
        }

        TextField {
            id: authenticationKey
            Layout.fillWidth: true
            echoMode: TextInput.Password
            text: "12345678"
        }

        Item {
            Layout.fillWidth: true
            height: childrenRect.height

            Button {
                anchors.right: parent.right
                text: qsTr("OK")

                onClicked: {
                    ApplicationSettings.serverUrl = serverUrl.text
                    ApplicationSettings.authenticationKey = authenticationKey.text

                    close()
                }
            }
        }
    }

    onClosed: {
        serverUrl.text = ApplicationSettings.serverUrl
        authenticationKey.text = "12345678"
    }
}
