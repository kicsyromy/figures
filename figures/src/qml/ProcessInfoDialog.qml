import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

import "components/helpers.js" as Js

Dialog {
    id: root
    modal: true
    focus: true

    property QtObject process: null

    onClosed: {
        process = null
    }

    title: process !== null ? process.name : ""

    contentHeight: column.height

    property int maximumTextLength: 0
    property var model: process !== null ? [
        [ qsTr("Command Line:"), process.commandLine ],
        [ qsTr("PID:"), process.pid ],
        [ qsTr("State:"), process.state ],
        [ qsTr("User:"), process.userName + " (" + process.userId + ")" ],
        [ qsTr("Group:"), process.groupName + " (" + process.groupId + ")" ],
        [ qsTr("CPU Usage:"), process.cpuUsage.toFixed(2) ],
        [ qsTr("RSS:"), Js.humanReadable(process.bytesRss) ],
        [ qsTr("PSS:"), Js.humanReadable(process.bytesPss) ],
        [ qsTr("Shared Memory:"), Js.humanReadable(process.bytesShared) ],
        [ qsTr("Private Memory:"), Js.humanReadable(process.bytesPrivate) ],
        [ qsTr("Swapped Out Memory:"), Js.humanReadable(process.bytesSwappedOut) ],
    ] : null

    ColumnLayout {
        id: column
        width: parent.width

        Repeater {
            model: root.model

            RowLayout {
                Layout.fillWidth: true

                Label {
                    Layout.minimumWidth: root.maximumTextLength
                    Layout.maximumWidth: root.maximumTextLength

                    onImplicitWidthChanged: {
                        if (implicitWidth + 10 > root.maximumTextLength) {
                            root.maximumTextLength = implicitWidth + 10
                        }
                    }

                    text: modelData[0]
                    elide: Text.ElideRight
                }

                Flickable {
                    Layout.fillWidth: true
                    height: label.implicitHeight

                    contentWidth: label.implicitWidth
                    contentHeight: label.implicitHeight

                    boundsMovement: Flickable.StopAtBounds
                    boundsBehavior: Flickable.DragOverBounds

                    clip: true

                    Label {
                        id: label

                        text: modelData[1]
                        elide: Text.ElideRight
                    }
                }
            }
        }
    }
}
