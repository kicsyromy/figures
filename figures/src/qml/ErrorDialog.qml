import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Dialog {
    id: root
    modal: true
    focus: true
    title: "Error occured"
    contentHeight: column.height

    property alias message: label.text

    Column {
        id: column
        spacing: 20

        Label {
            id: label
            width: root.availableWidth
            text: ""
            wrapMode: Label.Wrap
            font.pixelSize: 12
        }
    }
}
