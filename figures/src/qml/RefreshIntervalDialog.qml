import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Dialog {
    id: root
    modal: true
    focus: true
    title: qsTr("Refresh Interval")
    contentHeight: column.height

    ColumnLayout {
        id: column
        width: parent.width
        spacing: 10

        Label {
            Layout.fillWidth: true
            elide: Text.ElideRight
            text: qsTr("Refresh Interval (ms)")
        }

        TextField {
            id: refreshInterval
            Layout.fillWidth: true
            inputMask: "99000"
            cursorPosition: 0
            focus: true
            text: ApplicationSettings.refreshInterval
        }

        Item {
            Layout.fillWidth: true
            height: childrenRect.height

            Button {
                anchors.right: parent.right
                text: qsTr("OK")

                onClicked: {
                    ApplicationSettings.refreshInterval = refreshInterval.text

                    close()
                }
            }
        }
    }

    onClosed: {
        refreshInterval.text = ApplicationSettings.refreshInterval
    }
}
