import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

import "components" as Components
import "components/helpers.js" as Js

ListView {
    id: listView

    property StackView stackView
    property QtObject selectedDrive

    model: ServerDataModel.diskInfo.modelData

    Label {
        id: dummy
        width: 0
        height: 0
        visible: false
    }

    readonly property int textSize: 14

    boundsMovement: Flickable.StopAtBounds
    boundsBehavior: Flickable.DragOverBounds

    clip: true

    ScrollIndicator.vertical: ScrollIndicator { }

    delegate: ItemDelegate {
        width: listView.width
        height: diskInfo.isPhysicalDrive ? 80 : 50

        highlighted: ListView.isCurrentItem
        onClicked: {
            selectedDrive = diskInfo
            stackView.push(diskInfoDetails)
            listView.currentIndex = index
        }

        horizontalPadding: 20

        ColumnLayout {
            id: delegateRoot
            x: parent.horizontalPadding
            width: parent.width - x - parent.horizontalPadding

            spacing: 10

            anchors {
                verticalCenter: parent.verticalCenter
            }

            RowLayout {
                spacing: 10

                Label {
                    Layout.fillWidth: true

                    text: diskInfo.isPhysicalDrive ? diskInfo.model : qsTr("Logical Block Devices")
                    font.pointSize: textSize
                    elide: Text.ElideRight
                }

                Label {
                    visible: diskInfo.isPhysicalDrive
                    text: Js.humanReadable(diskInfo.capacity)
                    font.pointSize: textSize
                    elide: Text.ElideRight
                }
            }

            RowLayout {
                visible: diskInfo.isPhysicalDrive
                spacing: 10

                Label {
                    Layout.fillWidth: true

                    text: qsTr("S/N: ") + diskInfo.serialNumber
                    font.pointSize: textSize
                    elide: Text.ElideRight
                }

                Label {
                    text: qsTr("R: ") + Js.humanReadable(diskInfo.readSpeedBps) + qsTr("/s") + qsTr(" W: ") + Js.humanReadable(diskInfo.writeSpeedBps) + qsTr("/s")
                    font.pointSize: textSize
                    elide: Text.ElideRight
                }
            }
        }
    }

     Component {
        id: diskInfoDetails

        DiskInfoDetails {
            property string title: listView.selectedDrive.isPhysicalDrive ? listView.selectedDrive.model : qsTr("Logical Block Devices")

            selectedDrive: listView.selectedDrive
        }
    }
}
