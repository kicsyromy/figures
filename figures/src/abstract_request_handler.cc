#include "abstract_request_handler.hh"
#include "application_settings.hh"

#include <QGuiApplication>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QString>
#include <QStringBuilder>

namespace
{
    using RequestType = AbstractRequestHandler::RequestType;

    const auto REQUEST_TEMPLATE = QStringLiteral(R"({
    "key": "%1",
    "method": "%2",
    "parameters": [ )");

    const auto METHOD_CPU_USAGE = QStringLiteral("cpu_usage");
    const auto METHOD_MEMORY_USAGE = QStringLiteral("memory_usage");
    const auto METHOD_PROCESS_LIST = QStringLiteral("process_list");
    const auto METHOD_DISK_INFO = QStringLiteral("disk_info");

    QByteArray generateRequestBody(RequestType requestType,
        const QString &authenticationKey,
        const std::vector<std::string> &parameters)
    {
        auto request = REQUEST_TEMPLATE.arg(authenticationKey)
                           .arg([requestType] {
                               switch (requestType)
                               {
                               case RequestType::CpuUsage:
                                   return METHOD_CPU_USAGE;
                               case RequestType::MemoryUsage:
                                   return METHOD_MEMORY_USAGE;
                               case RequestType::ProcessList:
                                   return METHOD_PROCESS_LIST;
                               case RequestType::DiskInfo:
                                   return METHOD_DISK_INFO;
                               }
                               return QStringLiteral("");
                           }())
                           .toUtf8();

        for (const auto &parameter : parameters)
        {
            request.append('"');
            request.append(parameter.c_str(), parameter.size());
            request.append("\",");
        }

        request.remove(request.size() - 1, 1);
        request.append("]}", 2);

        return request;
    }
} // namespace

AbstractRequestHandler::AbstractRequestHandler(
    const std::shared_ptr<ApplicationSettings> &appSettings,
    const std::shared_ptr<QNetworkRequest> &server,
    const std::shared_ptr<QNetworkAccessManager> &networkAccessManager,
    RequestType requestType,
    std::vector<std::string> &&parameters,
    QObject *parent)
  : QObject(parent)
  , ongoingRequest_{ nullptr }
  , server_{ server }
  , networkAccessManager_{ networkAccessManager }
  , appSettings_{ appSettings }
  , refreshTimer_{}
  , jsonParser_{}
  , requestType_{ requestType }
  , requestParameters_{ std::move(parameters) }
{
    QObject::connect(&refreshTimer_, &QTimer::timeout, this, &AbstractRequestHandler::makeRequest);

    QObject::connect(appSettings.get(), &ApplicationSettings::refreshIntervalChanged, this, [this] {
        refreshTimer_.setInterval(appSettings_->refreshInterval());
    });

    QObject::connect(appSettings.get(),
        &ApplicationSettings::authenticationKeyChanged,
        this,
        [this] {
            requestString_ = generateRequestBody(requestType_,
                appSettings_->authenticationKey(),
                requestParameters_);
        });

    QObject::connect(appSettings.get(), &ApplicationSettings::serverUrlChanged, this, [this] {
        // setRefreshing(true);
    });

    refreshTimer_.setInterval(appSettings->refreshInterval());
    refreshTimer_.setSingleShot(true);

    requestString_ =
        generateRequestBody(requestType, appSettings->authenticationKey(), requestParameters_);
}

AbstractRequestHandler::~AbstractRequestHandler() = default;

void AbstractRequestHandler::makeRequest()
{
    if (ongoingRequest_ == nullptr)
    {
        ongoingRequest_ = networkAccessManager_->post(*server_.get(), requestString_);

        QObject::connect(ongoingRequest_,
            &QNetworkReply::finished,
            this,
            &AbstractRequestHandler::onRequestFinished);
    }
}

void AbstractRequestHandler::onRequestFinished()
{
    using namespace simdjson;

    if (ongoingRequest_->error() != QNetworkReply::NetworkError::NoError)
    {
        emit errorOccured(
            QStringLiteral("Failed to execute network request: ") + ongoingRequest_->errorString());

        ongoingRequest_->deleteLater();
        ongoingRequest_ = nullptr;

        setRefreshing(false);

        return;
    }

    auto response = ongoingRequest_->readAll();

    auto jsonDocument =
        jsonParser_.parse(response.data(), static_cast<std::size_t>(response.length()));
    ASSERT_PARSE_FAIL_MSG("Failed to parse response from server", jsonDocument);

    auto responseError = jsonDocument.get_object().at_key("error").get_string();
    ASSERT_PARSE_FAIL("response", responseError);

    if (responseError.value() != "")
    {
        errorOccured(QStringLiteral("Server replied with error message: %1")
                         .arg(responseError.value().data()));
        return;
    }

    auto responseJson = jsonDocument.get_object().at_key("response");
    ASSERT_PARSE_FAIL("response", responseJson);

    update(responseJson);

    if (refreshing_)
    {
        refreshTimer_.start();
    }
}
