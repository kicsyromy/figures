#include "application_settings.hh"

#include <QSettings>

namespace
{
    const auto DEFAULT_SERVER_ADDRESS = QStringLiteral("http://192.168.0.40:3000");
    const auto DEFAULT_SERVER_AUTH_KEY = QStringLiteral("dummy");
    const auto DEFAULT_REFRESH_INTERVAL_MS = 500;
    const auto DEFAULT_THEME = QStringLiteral("dark");

    const auto SECTION_SERVER = QStringLiteral("server");
    const auto SECTION_SERVER_KEY_ADDRESS = QStringLiteral("address");
    const auto SECTION_SERVER_KEY_AUTH_KEY = QStringLiteral("token");

    const auto SECTION_MISC = QStringLiteral("misc");
    const auto SECTION_MISC_KEY_REFRESH_INTERVAL = QStringLiteral("refresh");
    const auto SECTION_MISC_KEY_THEME = QStringLiteral("theme");
} // namespace

ApplicationSettings::ApplicationSettings(QObject *parent)
  : QObject(parent)
  , theme_{ DEFAULT_THEME }
  , serverUrl_{ DEFAULT_SERVER_ADDRESS }
  , authenticationKey_{ DEFAULT_SERVER_AUTH_KEY }
  , refreshInterval_{ DEFAULT_REFRESH_INTERVAL_MS }
{
    QSettings settings;

    settings.beginGroup(SECTION_SERVER);

    const auto serverUrl = settings.value(SECTION_SERVER_KEY_ADDRESS, QVariant{});
    if (!serverUrl.isValid())
    {
        settings.setValue(SECTION_SERVER_KEY_ADDRESS, DEFAULT_SERVER_ADDRESS);
    }
    else
    {
        serverUrl_ = serverUrl.toString();
    }

    const auto authenticationKey = settings.value(SECTION_SERVER_KEY_AUTH_KEY, QVariant{});
    if (!authenticationKey.isValid())
    {
        settings.setValue(SECTION_SERVER_KEY_AUTH_KEY, DEFAULT_SERVER_AUTH_KEY);
    }
    else
    {
        authenticationKey_ = authenticationKey.toString();
    }

    settings.endGroup();

    settings.beginGroup(SECTION_MISC);

    const auto refreshInterval = settings.value(SECTION_MISC_KEY_REFRESH_INTERVAL, QVariant{});
    if (!refreshInterval.isValid())
    {
        settings.setValue(SECTION_MISC_KEY_REFRESH_INTERVAL, DEFAULT_REFRESH_INTERVAL_MS);
    }
    else
    {
        refreshInterval_ = refreshInterval.toInt();
    }

    const auto theme = settings.value(SECTION_MISC_KEY_THEME, QVariant{});
    if (!theme.isValid())
    {
        settings.setValue(SECTION_MISC_KEY_THEME, DEFAULT_THEME);
    }
    else
    {
        theme_ = theme.toString();
    }

    settings.endGroup();
}

void ApplicationSettings::setTheme(const QString &value)
{
    if (theme_ != value)
    {
        theme_ = value;

        QSettings settings;
        settings.beginGroup(SECTION_MISC);
        settings.setValue(SECTION_MISC_KEY_THEME, value);
        settings.endGroup();

        emit themeChanged();
    }
}

void ApplicationSettings::setServerUrl(const QString &value)
{
    if (serverUrl_ != value)
    {
        serverUrl_ = value;

        QSettings settings;
        settings.beginGroup(SECTION_SERVER);
        settings.setValue(SECTION_SERVER_KEY_ADDRESS, value);
        settings.endGroup();

        emit serverUrlChanged();
    }
}

void ApplicationSettings::setAuthenticationKey(const QString &value)
{
    if (authenticationKey_ != value)
    {
        authenticationKey_ = value;

        QSettings settings;
        settings.beginGroup(SECTION_SERVER);
        settings.setValue(SECTION_SERVER_KEY_AUTH_KEY, value);
        settings.endGroup();

        emit authenticationKeyChanged();
    }
}

void ApplicationSettings::setRefreshInterval(int value)
{
    if (refreshInterval_ != value)
    {
        refreshInterval_ = value;

        QSettings settings;
        settings.beginGroup(SECTION_MISC);
        settings.setValue(SECTION_MISC_KEY_REFRESH_INTERVAL, value);
        settings.endGroup();

        emit refreshIntervalChanged();
    }
}
