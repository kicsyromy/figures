#include <simdjson/simdjson.h>

#include "process.hh"
#include "process_list_model.hh"

#include <QNetworkReply>
#include <QString>

#include <magic_enum/magic_enum.hpp>

namespace
{
    ProcessState::Value stateFromString(std::string_view value)
    {
        using namespace magic_enum;

        auto state = enum_cast<ProcessState::Value>(value);
        if (state.has_value())
        {
            return state.value();
        }

        return ProcessState::Unknown;
    }
} // namespace

ProcessListModel::ProcessListModel(const std::shared_ptr<ApplicationSettings> &appSettings,
    const std::shared_ptr<QNetworkRequest> &server,
    const std::shared_ptr<QNetworkAccessManager> &networkAccessManager,
    QObject *parent)
  : AbstractRequestHandler{ appSettings,
      server,
      networkAccessManager,
      AbstractRequestHandler::RequestType::ProcessList,
      { "filter=?!^kworker" },
      parent }
  , modelData_{ new ProcessListModelData{ this } }
  , modelFilter_{ new ProcessListSortFilterModel{ this } }
{
    QObject::connect(modelData_,
        &ProcessListModelData::errorOccured,
        this,
        &ProcessListModel::errorOccured,
        Qt::DirectConnection);

    QObject::connect(modelData_,
        &ProcessListModelData::coreCountChanged,
        this,
        &ProcessListModel::coreCountChanged,
        Qt::DirectConnection);

    QObject::connect(modelData_,
        &ProcessListModelData::totalMemoryChanged,
        this,
        &ProcessListModel::totalMemoryChanged,
        Qt::DirectConnection);

    modelFilter_->setSourceModel(modelData_);
    modelFilter_->setFilterRole(ProcessListModelData::RoleNames::ProcessInfo);
    modelFilter_->setSortRole(ProcessListModelData::RoleNames::ProcessInfo);
}

QSortFilterProxyModel *ProcessListModel::modelData()
{
    return modelFilter_;
}

qint64 ProcessListModel::coreCount() const
{
    return modelData_->coreCount();
}

qint64 ProcessListModel::totalMemory() const
{
    return modelData_->totalMemory();
}

void ProcessListModel::update(const simdjson::simdjson_result<simdjson::dom::element> &responseJson)
{
    modelData_->update(responseJson, ongoingRequest_);

    ongoingRequest_->deleteLater();
    ongoingRequest_ = nullptr;
}

int ProcessListModelData::rowCount([[maybe_unused]] const QModelIndex &parent) const
{
    return static_cast<int>(data_.size());
}

QVariant ProcessListModelData::data(const QModelIndex &i, int r) const
{
    const auto index = static_cast<std::size_t>(i.row());
    const auto role = static_cast<RoleNames>(r);

    switch (role)
    {
    case ProcessInfo:
        return QVariant::fromValue(static_cast<QObject *>(data_[index].second.get()));
    }

    return QVariant();
}

QHash<int, QByteArray> ProcessListModelData::roleNames() const
{
    return roleNames_;
}

void ProcessListModelData::update(
    const simdjson::simdjson_result<simdjson::dom::element> &responseJson,
    QNetworkReply *reply)
{
    using namespace simdjson;

    // Ugh... done to satisfy the macros used for parse error handling
    auto ongoingRequest_ = reply;

    auto responseObject = responseJson.get_object();
    ASSERT_PARSE_FAIL("reponse(process_list)", responseObject);

    auto coreCount = responseObject.at_key("core_count").get_int64();
    ASSERT_PARSE_FAIL("core_count", coreCount);

    if (coreCount_ != coreCount.value())
    {
        coreCount_ = coreCount.value();
        emit coreCountChanged();
    }

    auto totalMemory = responseObject.at_key("total_memory").get_int64();
    ASSERT_PARSE_FAIL("total_memory", totalMemory);

    if (totalMemory_ != totalMemory.value())
    {
        totalMemory_ = totalMemory.value();
        emit totalMemoryChanged();
    }

    auto processList = responseObject.at_key("process_list").get_array();
    ASSERT_PARSE_FAIL("process_list", processList);

    if (data_.empty())
    {
        data_.reserve(processList.size());
        toRemove_.reserve(processList.size());
    }

    for (const auto &process : processList)
    {
        auto name = process.at_key("name").get_string();
        ASSERT_PARSE_FAIL("name", name);

        auto commandLine = process.at_key("command_line").get_string();
        ASSERT_PARSE_FAIL("command_line", commandLine);

        auto pid = process.at_key("pid").get_int64();
        ASSERT_PARSE_FAIL("pid", pid);

        auto state = process.at_key("state").get_string();
        ASSERT_PARSE_FAIL("state", state);

        auto userId = process.at_key("user_id").get_int64();
        ASSERT_PARSE_FAIL("user_id", userId);

        auto groupId = process.at_key("group_id").get_int64();
        ASSERT_PARSE_FAIL("group_id", groupId);

        auto userName = process.at_key("user_name").get_string();
        ASSERT_PARSE_FAIL("user_name", userName);

        auto groupName = process.at_key("group_name").get_string();
        ASSERT_PARSE_FAIL("group_name", groupName);

        auto memoryRss = process.at_key("memory_rss").get_int64();
        ASSERT_PARSE_FAIL("memory_rss", memoryRss);

        auto memoryPss = process.at_key("memory_pss").get_int64();
        ASSERT_PARSE_FAIL("memory_pss", memoryPss);

        auto memoryShared = process.at_key("memory_shared").get_int64();
        ASSERT_PARSE_FAIL("memory_shared", memoryShared);

        auto memoryPrivate = process.at_key("memory_private").get_int64();
        ASSERT_PARSE_FAIL("memory_private", memoryPrivate);

        auto memorySwappedOut = process.at_key("memory_swapped_out").get_int64();
        ASSERT_PARSE_FAIL("memory_swapped_out", memorySwappedOut);

        auto cpuUsage = process.at_key("cpu_usage_percent").get_double();
        ASSERT_PARSE_FAIL("cpu_usage_precent", cpuUsage);

        auto parsedName = QString::fromLocal8Bit(name.value().data(), name.value().size());

        auto it = std::find_if(data_.begin(),
            data_.end(),
            [&parsedName, pid = pid.value()](const auto &process) {
                return process.second->name() == parsedName && process.second->pid() == pid;
            });

        if (it != data_.end())
        {
            auto &proc = *it;
            proc.first = true;
            proc.second->setState(stateFromString(state.value()));
            proc.second->setBytesRss(memoryRss.value());
            proc.second->setBytesPss(memoryPss.value());
            proc.second->setBytesShared(memoryShared.value());
            proc.second->setBytesPrivate(memoryPrivate.value());
            proc.second->setBytesSwappedOut(memorySwappedOut.value());
            proc.second->setCpuUsage(cpuUsage.value());
        }
        else
        {
            // Normally this is not needed, we should always insert new processes at the end
            // buuuut just in case something wacky happens, this search is done for every new
            // process
            auto newPosition =
                std::find_if(data_.begin(), data_.end(), [pid = pid.value()](const auto &process) {
                    return process.second->pid() > pid;
                });

            auto proc = std::make_unique<Process>();
            proc->setName(std::move(parsedName));
            proc->setCommandLine(
                QString::fromLocal8Bit(commandLine.value().data(), commandLine.value().size()));
            proc->setPid(pid.value());
            proc->setState(stateFromString(state.value()));
            proc->setUserId(userId.value());
            proc->setGroupId(groupId.value());
            proc->setUserName(
                QString::fromLocal8Bit(userName.value().data(), userName.value().size()));
            proc->setGroupName(
                QString::fromLocal8Bit(groupName.value().data(), groupName.value().size()));
            proc->setBytesRss(memoryRss.value());
            proc->setBytesPss(memoryPss.value());
            proc->setBytesShared(memoryShared.value());
            proc->setBytesPrivate(memoryPrivate.value());
            proc->setBytesSwappedOut(memorySwappedOut.value());
            proc->setCpuUsage(cpuUsage.value());

            const auto newIndex = static_cast<int>(newPosition - data_.begin());
            beginInsertRows(QModelIndex{}, newIndex, newIndex);
            {
                data_.insert(newPosition, { true, std::move(proc) });
            }
            endInsertRows();
        }
    }

    for (auto it = data_.begin(); it != data_.end(); ++it)
    {
        if (!it->first)
            toRemove_.push_back(it);
    }

    int itOffset = 0;
    for (auto it = toRemove_.begin(); it != toRemove_.end(); ++it)
    {
        const auto dataIt = ((*it) - itOffset);
        const auto processIndex = static_cast<int>(dataIt - data_.begin());
        beginRemoveRows(QModelIndex{}, processIndex, processIndex);
        {
            data_.erase(dataIt);
        }
        endRemoveRows();

        ++itOffset;
    }

    toRemove_.clear();

    for (auto &[updated, _] : data_)
    {
        updated = false;
    }
}

ProcessListSortFilterModel::ProcessListSortFilterModel(QObject *parent)
  : QSortFilterProxyModel{ parent }
{
    setDynamicSortFilter(true);
    setFilterCaseSensitivity(Qt::CaseSensitivity::CaseInsensitive);
}

bool ProcessListSortFilterModel::filterAcceptsRow(int sourceRow,
    const QModelIndex &sourceParent) const
{
    const auto index = sourceModel()->index(sourceRow, 0, sourceParent);
    const auto process =
        static_cast<Process *>(qvariant_cast<QObject *>(sourceModel()->data(index, filterRole())));

    const auto regexp = filterRegularExpression();
    return process->name().contains(regexp) || process->commandLine().contains(regexp) ||
           process->userName().contains(regexp) || process->groupName().contains(regexp);
}

bool ProcessListSortFilterModel::lessThan(const QModelIndex &sourceLeft,
    const QModelIndex &sourceRight) const
{
    const auto lhs = static_cast<Process *>(
        qvariant_cast<QObject *>(sourceModel()->data(sourceLeft, sortRole())));

    const auto rhs = static_cast<Process *>(
        qvariant_cast<QObject *>(sourceModel()->data(sourceRight, sortRole())));

    return lhs->cpuUsage() < rhs->cpuUsage();
}
