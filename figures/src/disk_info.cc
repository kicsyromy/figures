#include "disk_info.hh"

#include "block_device_model.hh"

Drive::Drive(QObject *parent)
  : QObject(parent)
  , blockDevices_{ new BlockDeviceListModel{ this } }
{}

float Drive::readSpeedBps() const
{
    if (!isPhysicalDrive_)
    {
        return -1.f;
    }

    float result{ 0.f };

    const auto &blockDevices = static_cast<const BlockDeviceListModel *>(blockDevices_)->devices();
    for (std::size_t i = 1; i < blockDevices.size(); ++i)
    {
        result += blockDevices[i].second->readSpeedBps();
    }

    return result;
}

float Drive::writeSpeedBps() const
{
    if (!isPhysicalDrive_)
    {
        return -1.f;
    }

    float result{ 0.f };

    const auto &blockDevices = static_cast<const BlockDeviceListModel *>(blockDevices_)->devices();
    for (std::size_t i = 1; i < blockDevices.size(); ++i)
    {
        result += blockDevices[i].second->writeSpeedBps();
    }

    return result;
}
