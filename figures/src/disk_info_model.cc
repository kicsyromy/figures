#include <simdjson/simdjson.h>

#include "block_device_model.hh"
#include "disk_info_model.hh"

#include <QNetworkReply>

DiskInfoModel::DiskInfoModel(const std::shared_ptr<ApplicationSettings> &appSettings,
    const std::shared_ptr<QNetworkRequest> &server,
    const std::shared_ptr<QNetworkAccessManager> &networkAccessManager,
    QObject *parent)
  : AbstractRequestHandler{ appSettings,
      server,
      networkAccessManager,
      AbstractRequestHandler::RequestType::DiskInfo,
      {},
      parent }
  , modelData_{ new DiskInfoModelData{ this } }
{
    QObject::connect(modelData_,
        &DiskInfoModelData::errorOccured,
        this,
        &DiskInfoModel::errorOccured,
        Qt::DirectConnection);
}

QAbstractListModel *DiskInfoModel::modelData()
{
    return modelData_;
}

DiskInfoModel::~DiskInfoModel() = default;

void DiskInfoModel::update(const simdjson::simdjson_result<simdjson::dom::element> &responseJson)
{
    modelData_->update(responseJson, ongoingRequest_);

    ongoingRequest_->deleteLater();
    ongoingRequest_ = nullptr;
}

int DiskInfoModelData::rowCount([[maybe_unused]] const QModelIndex &parent) const
{
    return static_cast<int>(data_.size());
}

QVariant DiskInfoModelData::data(const QModelIndex &i, int r) const
{
    const auto index = static_cast<std::size_t>(i.row());
    const auto role = static_cast<RoleNames>(r);

    switch (role)
    {
    case DiskInfo:
        return QVariant::fromValue(static_cast<QObject *>(data_[index].second.get()));
    }

    return QVariant();
}

QHash<int, QByteArray> DiskInfoModelData::roleNames() const
{
    return roleNames_;
}

void DiskInfoModelData::update(
    const simdjson::simdjson_result<simdjson::dom::element> &responseJson,
    QNetworkReply *reply)
{
    using namespace simdjson;

    // Ugh... done to satisfy the macros used for parse error handling
    auto ongoingRequest_ = reply;

    auto driveArray = responseJson.get_array();
    ASSERT_PARSE_FAIL("response(disk_info)", driveArray);

    for (const auto &drive : driveArray)
    {
        auto isPhysicalDevice = drive.at_key("is_physical_drive").get_bool();
        ASSERT_PARSE_FAIL("is_physical_drive", isPhysicalDevice);

        auto model = drive.at_key("model").get_string();
        ASSERT_PARSE_FAIL("model", model);

        auto serialNumber = drive.at_key("serial_no").get_string();
        ASSERT_PARSE_FAIL("serial_no", serialNumber);

        auto capacity = drive.at_key("capacity").get_uint64();
        ASSERT_PARSE_FAIL("capacity", capacity);

        auto blockDevices = drive.at_key("block_devices").get_array();
        ASSERT_PARSE_FAIL("block_devices", blockDevices);

        auto hasSmartInfo = drive.at_key("has_smart_info").get_bool();
        ASSERT_PARSE_FAIL("has_smart_info", hasSmartInfo);

        auto temperature = drive.at_key("temperature_celsius").get_double();
        ASSERT_PARSE_FAIL("temperature_celsius", temperature);

        auto smartStatus = drive.at_key("smart_status").get_string();
        ASSERT_PARSE_FAIL("smart_status", smartStatus);

        auto parsedSerialNumber =
            QString::fromLocal8Bit(serialNumber.value().data(), serialNumber.value().size());

        auto it =
            std::find_if(data_.begin(), data_.end(), [&parsedSerialNumber](const auto &diskInfo) {
                return diskInfo.second->serialNumber() == parsedSerialNumber;
            });

        Drive *driveEntry = nullptr;
        if (it != data_.end())
        {
            auto &drive = *it;

            drive.first = true;

            drive.second->setHasSmartInfo(hasSmartInfo.value());
            drive.second->setTemperature(temperature.value());
            drive.second->setSmartStatus(
                QString::fromLocal8Bit(smartStatus.value().data(), smartStatus.value().size()));

            driveEntry = drive.second.get();
        }
        else
        {
            auto parsedModel = QString::fromLocal8Bit(model.value().data(), model.value().size());
            auto newPosition = std::find_if(data_.begin(),
                data_.end(),
                [parsedModel = &parsedModel](const auto &drive) {
                    return drive.second->model() > *parsedModel;
                });

            auto drive = std::make_unique<class Drive>();
            drive->setIsPhysicalDrive(isPhysicalDevice.value());
            drive->setModel(parsedModel);
            drive->setSerialNumber(parsedSerialNumber);
            drive->setCapacity(capacity.value());
            drive->setHasSmartInfo(hasSmartInfo.value());
            drive->setTemperature(temperature.value());
            drive->setSmartStatus(
                QString::fromLocal8Bit(smartStatus.value().data(), smartStatus.value().size()));

            const auto newIndex = static_cast<int>(newPosition - data_.begin());
            beginInsertRows(QModelIndex{}, newIndex, newIndex);
            {
                auto it = data_.insert(newPosition, { true, std::move(drive) });
                driveEntry = it->second.get();
            }
            endInsertRows();
        }

        for (const auto &blockDevice : blockDevices)
        {
            auto block = BlockDevice{};

            auto deviceFile = blockDevice.at_key("device_file").get_string();
            ASSERT_PARSE_FAIL("device_file", deviceFile);
            block.setDeviceFile(
                QString::fromLocal8Bit(deviceFile.value().data(), deviceFile.value().size()));

            auto size = blockDevice.at_key("size").get_uint64();
            ASSERT_PARSE_FAIL("size", size);
            block.setSizeBytes(size.value());

            auto label = blockDevice.at_key("label").get_string();
            ASSERT_PARSE_FAIL("label", label);
            block.setLabel(QString::fromLocal8Bit(label.value().data(), label.value().size()));

            auto uuid = blockDevice.at_key("uuid").get_string();
            ASSERT_PARSE_FAIL("uuid", uuid);
            block.setUuid(QString::fromLocal8Bit(uuid.value().data(), uuid.value().size()));

            if (!blockDevice.at_key("filesystem").is_null())
            {
                auto filesystem = blockDevice.at_key("filesystem").get_object();
                ASSERT_PARSE_FAIL("filesystem", filesystem);

                auto *fs = static_cast<Filesystem *>(block.filesystem());
                fs->mountPoints_.clear();

                auto fsMountPoints = filesystem.at_key("mount_points").get_array();
                ASSERT_PARSE_FAIL("mount_points", fsMountPoints);
                QList<QString> mountPoints{};
                for (const auto &mountPoint : fsMountPoints)
                {
                    auto value = mountPoint.get_string();
                    ASSERT_PARSE_FAIL("mount_point", value);
                    mountPoints.append(
                        QString::fromLocal8Bit(value.value().data(), value.value().size()));
                }
                fs->setMountPoints(std::move(mountPoints));

                auto fsFilesystem = filesystem.at_key("filesystem").get_string();
                ASSERT_PARSE_FAIL("filesystem", fsFilesystem);
                fs->setFilesystem(QString::fromLocal8Bit(fsFilesystem.value().data(),
                    fsFilesystem.value().size()));

                auto fsSize = filesystem.at_key("size_bytes").get_uint64();
                ASSERT_PARSE_FAIL("size_bytes", fsSize);
                fs->setSizeBytes(fsSize.value());

                auto fsUsed = filesystem.at_key("used_bytes").get_uint64();
                ASSERT_PARSE_FAIL("used_bytes", fsUsed);
                fs->setUsedBytes(fsUsed.value());
            }

            auto readSpeed = blockDevice.at_key("read_speed_bps").get_double();
            ASSERT_PARSE_FAIL("read_speed_bps", readSpeed);
            block.setReadSpeedBps(readSpeed.value());

            auto writeSpeed = blockDevice.at_key("write_speed_bps").get_double();
            ASSERT_PARSE_FAIL("write_speed_bps", writeSpeed);
            block.setWriteSpeedBps(writeSpeed.value());

            static_cast<BlockDeviceListModel *>(driveEntry->blockDevices())
                ->addOrUpdate(std::move(block));
            static_cast<BlockDeviceListModel *>(driveEntry->blockDevices())
                ->setIsPhysicalDrive(driveEntry->isPhysicalDrive());
        }
    }

    for (auto it = data_.begin(); it != data_.end(); ++it)
    {
        if (!it->first)
        {
            toRemove_.push_back(it);
        }
        else
        {
            emit it->second->readSpeedBpsChanged();
            emit it->second->writeSpeedBpsChanged();
        }
    }

    int itOffset = 0;
    for (auto it = toRemove_.begin(); it != toRemove_.end(); ++it)
    {
        const auto dataIt = ((*it) - itOffset);
        const auto processIndex = static_cast<int>(dataIt - data_.begin());
        beginRemoveRows(QModelIndex{}, processIndex, processIndex);
        {
            data_.erase(dataIt);
        }
        endRemoveRows();

        ++itOffset;
    }

    toRemove_.clear();

    for (auto &[updated, _] : data_)
    {
        updated = false;
    }
}
