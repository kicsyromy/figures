#pragma once

#include "abstract_request_handler.hh"
#include "process.hh"

#include <vector>

#include <QAbstractListModel>
#include <QSortFilterProxyModel>

class ProcessListModel final : public AbstractRequestHandler
{
    Q_OBJECT
    Q_PROPERTY(QSortFilterProxyModel *modelData READ modelData CONSTANT)
    Q_PROPERTY(qint64 coreCount READ coreCount NOTIFY coreCountChanged)
    Q_PROPERTY(qint64 totalMemory READ totalMemory NOTIFY totalMemoryChanged)

public:
    ProcessListModel(const std::shared_ptr<ApplicationSettings> &appSettings,
        const std::shared_ptr<QNetworkRequest> &server,
        const std::shared_ptr<QNetworkAccessManager> &networkAccessManager,
        QObject *parent = nullptr);

public:
    QSortFilterProxyModel *modelData();
    qint64 coreCount() const;
    qint64 totalMemory() const;

signals:
    void coreCountChanged();
    void totalMemoryChanged();

private:
    void update(const simdjson::simdjson_result<simdjson::dom::element> &responseJson) override;

private:
    class ProcessListModelData *modelData_{ nullptr };
    class ProcessListSortFilterModel *modelFilter_{ nullptr };
};

class ProcessListModelData final : public QAbstractListModel
{
    Q_OBJECT

public:
    enum RoleNames
    {
        ProcessInfo = Qt::DisplayRole + 1,
    };

public:
    ProcessListModelData(QObject *parent = nullptr)
      : QAbstractListModel{ parent }
      , roleNames_({
            std::pair<int, QByteArray>{ RoleNames::ProcessInfo, "processInfo" },
        })
    {}

public:
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

public:
    qint64 coreCount() const
    {
        return coreCount_;
    }

    qint64 totalMemory() const
    {
        return totalMemory_;
    }

public:
    void update(const simdjson::simdjson_result<simdjson::dom::element> &responseJson,
        QNetworkReply *reply);

signals:
    void errorOccured(QString message);
    void coreCountChanged();
    void totalMemoryChanged();

private:
    QHash<int, QByteArray> roleNames_;

    std::vector<std::pair<bool, std::unique_ptr<Process>>> data_{};
    std::vector<std::decay_t<decltype(data_)>::iterator> toRemove_{};
    qint64 coreCount_{ 0 };
    qint64 totalMemory_{ 0 };
};

class ProcessListSortFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    ProcessListSortFilterModel(QObject *parent = nullptr);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
    bool lessThan(const QModelIndex &sourceLeft, const QModelIndex &sourceRight) const override;
};
