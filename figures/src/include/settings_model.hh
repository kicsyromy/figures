#pragma once

#include <QList>
#include <QObject>

class SettingsEntry : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString icon MEMBER icon_ CONSTANT)
    Q_PROPERTY(QString name MEMBER name_ CONSTANT)

public:
    SettingsEntry(const char *icon, const char *name, QObject *parent = nullptr)
      : QObject(parent)
      , icon_{ icon }
      , name_{ name }
    {}

private:
    QString icon_;
    QString name_;
};

class SettingsSection : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name MEMBER name_ CONSTANT)
    Q_PROPERTY(QList<QObject *> entries MEMBER entries_ CONSTANT)

public:
    template<typename... Entries>
    SettingsSection(const char *name, QObject *parent, Entries &&...entries)
      : QObject(parent)
      , name_{ name }
      , entries_{ std::forward<Entries>(entries)... }
    {
        static_assert(std::is_same_v<std::common_type_t<Entries...>, SettingsEntry *>);
    }

private:
    QString name_;
    QList<QObject *> entries_;
};

class SettingsSections
{
    Q_GADGET

public:
    enum Value
    {
        General,
        Count
    };
    Q_ENUM(Value)
};

class SettingsModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<QObject *> items MEMBER items_ CONSTANT)

public:
    SettingsModel(QObject *parent = nullptr);
    ~SettingsModel();

private:
    QList<QObject *> items_;
};
