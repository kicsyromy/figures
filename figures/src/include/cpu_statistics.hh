#pragma once

#include <QObject>

class CpuStatistics : public QObject
{
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(QString processor            READ processor            NOTIFY processorChanged)
    Q_PROPERTY(int     physicalId           READ physicalId           NOTIFY physicalIdChanged)
    Q_PROPERTY(int     logicalId            READ logicalId            NOTIFY logicalIdChanged)
    Q_PROPERTY(int     activePercent        READ activePercent        NOTIFY activePercentChanged)
    Q_PROPERTY(int     idlePercent          READ idlePercent          NOTIFY idlePercentChanged)
    Q_PROPERTY(int     frequency            READ frequency            NOTIFY frequencyChanged)
    Q_PROPERTY(int     temperatureCurrent   READ temperatureCurrent   NOTIFY temperatureCurrentChanged)
    Q_PROPERTY(int     temperatureThreshold READ temperatureThreshold NOTIFY temperatureThresholdChanged)
    // clang-format on

public:
    CpuStatistics(QObject *parent = nullptr)
      : QObject(parent)
    {}

public:
    inline QString processor() const
    {
        return processor_;
    }
    constexpr int physicalId() const
    {
        return physicalId_;
    }
    constexpr int logicalId() const
    {
        return logicalId_;
    }
    constexpr int activePercent() const
    {
        return activePercent_;
    }
    constexpr int idlePercent() const
    {
        return idlePercent_;
    }
    constexpr int frequency() const
    {
        return frequency_;
    }
    constexpr int temperatureCurrent() const
    {
        return temperatureCurrent_;
    }
    constexpr int temperatureThreshold() const
    {
        return temperatureThreshold_;
    }

signals:
    void processorChanged();
    void physicalIdChanged();
    void logicalIdChanged();
    void activePercentChanged();
    void idlePercentChanged();
    void frequencyChanged();
    void temperatureCurrentChanged();
    void temperatureThresholdChanged();

private:
    void setProcessor(const QString &value)
    {
        if (processor_ != value)
        {
            processor_ = value;
            emit processorChanged();
        }
    }

    void setPhysicalId(int value)
    {
        if (physicalId_ != value)
        {
            physicalId_ = value;
            emit physicalIdChanged();
        }
    }

    void setLogicalId(int value)
    {
        if (logicalId_ != value)
        {
            logicalId_ = value;
            emit logicalIdChanged();
        }
    }

    void setActivePercent(int value)
    {
        if (activePercent_ != value)
        {
            activePercent_ = value;
            emit activePercentChanged();
        }
    }

    void setIdlePercent(int value)
    {
        if (idlePercent_ != value)
        {
            idlePercent_ = value;
            emit idlePercentChanged();
        }
    }

    void setFrequency(int value)
    {
        if (frequency_ != value)
        {
            frequency_ = value;
            emit frequencyChanged();
        }
    }

    void setTemperatureCurrent(int value)
    {
        if (temperatureCurrent_ != value)
        {
            temperatureCurrent_ = value;
            emit temperatureCurrentChanged();
        }
    }

    void setTemperatureThreshold(int value)
    {
        if (temperatureThreshold_ != value)
        {
            temperatureThreshold_ = value;
            emit temperatureThresholdChanged();
        }
    }

private:
    QString processor_{};
    int physicalId_{ -1 };
    int logicalId_{ -1 };
    int activePercent_{ -1 };
    int idlePercent_{ -1 };
    int frequency_{ -1 };
    int temperatureCurrent_{ -1 };
    int temperatureThreshold_{ -1 };

private:
    friend class CpuStatisticsModel;
};
