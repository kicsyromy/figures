#pragma once

#include "abstract_request_handler.hh"

class Overview final : public AbstractRequestHandler
{
    Q_OBJECT
    // clang-format off
//    Q_PROPERTY(QString cpuName READ cpuName NOTIFY cpuNameChanged)
//    Q_PROPERTY(float cpuActivePercent READ cpuActivePercent NOTIFY cpuActivePercentChanged)
//    Q_PROPERTY(float cpuFrequency READ cpuFrequency NOTIFY cpuFrequencyChanged)
//    Q_PROPERTY(quint64 memTotalBytes READ memTotalBytes NOTIFY memTotalBytesChanged)
//    Q_PROPERTY(quint64 memUsedBytes READ memUsedBytes NOTIFY memUsedBytesChanged)
    // clang-format on

public:
    Overview(const std::shared_ptr<ApplicationSettings> &appSettings,
        const std::shared_ptr<QNetworkRequest> &server,
        const std::shared_ptr<QNetworkAccessManager> &networkAccessManager,
        QObject *parent = nullptr);
    ~Overview();

signals:
    void bytesTotalChanged();
    void bytesUsedChanged();
    void swapBytesTotalChanged();
    void swapBytesUsedChanged();

private:
    void update(const simdjson::simdjson_result<simdjson::dom::element> &responseJson) override;

private:
    qint64 bytesTotal_{ -1 };
    qint64 bytesUsed_{ -1 };
    qint64 swapBytesTotal_{ -1 };
    qint64 swapBytesUsed_{ -1 };
};
