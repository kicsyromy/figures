#pragma once

#include "abstract_request_handler.hh"

class MemoryStatistics final : public AbstractRequestHandler
{
    Q_OBJECT
    Q_PROPERTY(qint64 bytesTotal MEMBER bytesTotal_ NOTIFY bytesTotalChanged)
    Q_PROPERTY(qint64 bytesUsed MEMBER bytesUsed_ NOTIFY bytesUsedChanged)
    Q_PROPERTY(qint64 swapBytesTotal MEMBER swapBytesTotal_ NOTIFY swapBytesTotalChanged)
    Q_PROPERTY(qint64 swapBytesUsed MEMBER swapBytesUsed_ NOTIFY swapBytesUsedChanged)

public:
    MemoryStatistics(const std::shared_ptr<ApplicationSettings> &appSettings,
        const std::shared_ptr<QNetworkRequest> &server,
        const std::shared_ptr<QNetworkAccessManager> &networkAccessManager,
        QObject *parent = nullptr);
    ~MemoryStatistics();

signals:
    void bytesTotalChanged();
    void bytesUsedChanged();
    void swapBytesTotalChanged();
    void swapBytesUsedChanged();

private:
    void update(const simdjson::simdjson_result<simdjson::dom::element> &responseJson) override;

private:
    qint64 bytesTotal_{ -1 };
    qint64 bytesUsed_{ -1 };
    qint64 swapBytesTotal_{ -1 };
    qint64 swapBytesUsed_{ -1 };
};
