#pragma once

#include <magic_enum/magic_enum.hpp>

#include <QObject>

class ProcessState
{
    Q_GADGET

public:
    enum Value
    {
        Running,
        Sleeping,
        Waiting,
        Zombie,
        Stopped,
        Tracing,
        Dead,
        Unknown,
    };
    Q_ENUM(Value)
};

class Process : public QObject
{
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(QString name            READ name            NOTIFY nameChanged)
    Q_PROPERTY(QString commandLine     READ commandLine     NOTIFY commandLineChanged)
    Q_PROPERTY(qint64  pid             READ pid             NOTIFY pidChanged)
    Q_PROPERTY(QString state           READ state           NOTIFY stateChanged)
    Q_PROPERTY(qint64  userId          READ userId          NOTIFY userIdChanged)
    Q_PROPERTY(qint64  groupId         READ groupId         NOTIFY groupIdChanged)
    Q_PROPERTY(QString userName        READ userName        NOTIFY userNameChanged)
    Q_PROPERTY(QString groupName       READ groupName       NOTIFY groupNameChanged)
    Q_PROPERTY(qint64  bytesRss        READ bytesRss        NOTIFY bytesRssChanged)
    Q_PROPERTY(qint64  bytesPss        READ bytesPss        NOTIFY bytesPssChanged)
    Q_PROPERTY(qint64  bytesShared     READ bytesShared     NOTIFY bytesSharedChanged)
    Q_PROPERTY(qint64  bytesPrivate    READ bytesPrivate    NOTIFY bytesPrivateChanged)
    Q_PROPERTY(qint64  bytesSwappedOut READ bytesSwappedOut NOTIFY bytesSwappedOutChanged)
    Q_PROPERTY(float   cpuUsage        READ cpuUsage        NOTIFY cpuUsageChanged)
    // clang-format on

public:
    Process(QObject *parent = nullptr)
      : QObject(parent)
    {}

public:
    QString name() const
    {
        return name_;
    }

    QString commandLine() const
    {
        return commandLine_;
    }

    qint64 pid() const
    {
        return pid_;
    }

    QString state() const
    {
        using namespace magic_enum;
        auto stateString = enum_name<ProcessState::Value>(state_);
        return QString::fromLocal8Bit(stateString.data(), stateString.size());
    }

    qint64 userId() const
    {
        return userId_;
    }

    qint64 groupId() const
    {
        return groupId_;
    }

    QString userName() const
    {
        return userName_;
    }

    QString groupName() const
    {
        return groupName_;
    }

    qint64 bytesRss() const
    {
        return bytesRss_;
    }

    qint64 bytesPss() const
    {
        return bytesPss_;
    }

    qint64 bytesShared() const
    {
        return bytesShared_;
    }

    qint64 bytesPrivate() const
    {
        return bytesPrivate_;
    }

    qint64 bytesSwappedOut() const
    {
        return bytesSwappedOut_;
    }

    float cpuUsage() const
    {
        return cpuUsage_;
    }

signals:
    void nameChanged();
    void commandLineChanged();
    void pidChanged();
    void stateChanged();
    void userIdChanged();
    void groupIdChanged();
    void userNameChanged();
    void groupNameChanged();
    void bytesRssChanged();
    void bytesPssChanged();
    void bytesSharedChanged();
    void bytesPrivateChanged();
    void bytesSwappedOutChanged();
    void cpuUsageChanged();

private:
    void setName(QString &&value)
    {
        if (name_ != value)
        {
            name_ = std::move(value);
            emit nameChanged();
        }
    }

    void setName(const QString &value)
    {
        if (name_ != value)
        {
            name_ = value;
            emit nameChanged();
        }
    }

    void setCommandLine(QString &&value)
    {
        if (commandLine_ != value)
        {
            commandLine_ = std::move(value);
            emit commandLineChanged();
        }
    }

    void setCommandLine(const QString &value)
    {
        if (commandLine_ != value)
        {
            commandLine_ = value;
            emit commandLineChanged();
        }
    }

    void setPid(qint64 value)
    {
        if (pid_ != value)
        {
            pid_ = value;
            emit pidChanged();
        }
    }

    void setState(int value)
    {
        if (state_ != value && value > 0 && value <= ProcessState::Unknown)
        {
            state_ = static_cast<ProcessState::Value>(value);
            emit stateChanged();
        }
    }

    void setUserId(qint64 value)
    {
        if (userId_ != value)
        {
            userId_ = value;
            emit userIdChanged();
        }
    }

    void setGroupId(qint64 value)
    {
        if (groupId_ != value)
        {
            groupId_ = value;
            emit groupIdChanged();
        }
    }

    void setUserName(const QString &value)
    {
        if (userName_ != value)
        {
            userName_ = value;
            emit userNameChanged();
        }
    }

    void setGroupName(const QString &value)
    {
        if (groupName_ != value)
        {
            groupName_ = value;
            emit groupNameChanged();
        }
    }

    void setUserName(QString &&value)
    {
        if (userName_ != value)
        {
            userName_ = std::move(value);
            emit userNameChanged();
        }
    }

    void setBytesRss(qint64 value)
    {
        if (bytesRss_ != value)
        {
            bytesRss_ = value;
            emit bytesRssChanged();
        }
    }

    void setBytesPss(qint64 value)
    {
        if (bytesPss_ != value)
        {
            bytesPss_ = value;
            emit bytesPssChanged();
        }
    }

    void setBytesShared(qint64 value)
    {
        if (bytesShared_ != value)
        {
            bytesShared_ = value;
            emit bytesSharedChanged();
        }
    }

    void setBytesPrivate(qint64 value)
    {
        if (bytesPrivate_ != value)
        {
            bytesPrivate_ = value;
            emit bytesPrivateChanged();
        }
    }

    void setBytesSwappedOut(qint64 value)
    {
        if (bytesSwappedOut_ != value)
        {
            bytesSwappedOut_ = value;
            emit bytesSwappedOutChanged();
        }
    }

    void setCpuUsage(float value)
    {
        if (std::abs(cpuUsage_ - value) > 0.001)
        {
            cpuUsage_ = value;
            emit cpuUsageChanged();
        }
    }

private:
    QString name_{};
    QString commandLine_{};
    qint64 pid_{ -1 };
    ProcessState::Value state_{ ProcessState::Unknown };
    qint64 userId_{ -1 };
    qint64 groupId_{ -1 };
    QString userName_{};
    QString groupName_{};
    qint64 bytesRss_{ -1 };
    qint64 bytesPss_{ -1 };
    qint64 bytesShared_{ -1 };
    qint64 bytesPrivate_{ -1 };
    qint64 bytesSwappedOut_{ -1 };
    float cpuUsage_{ 0.0 };

private:
    friend class ProcessListModel;
    friend class ProcessListModelData;
};
