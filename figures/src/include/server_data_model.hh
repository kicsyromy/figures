#pragma once

#include "application_settings.hh"
#include "cpu_statistics_model.hh"
#include "disk_info_model.hh"
#include "memory_statistics.hh"
#include "process_list_model.hh"

#include <memory>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QObject>

class QGuiApplication;

namespace simdjson::dom
{
    class parser;
}

class ServerDataModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QObject *cpuStatistics READ cpuStatistics CONSTANT)
    Q_PROPERTY(QObject *memoryStatistics READ memoryStatistics CONSTANT)
    Q_PROPERTY(QObject *processList READ processList CONSTANT)
    Q_PROPERTY(QObject *diskInfo READ diskInfo CONSTANT)

public:
    ServerDataModel(QGuiApplication *app,
        const std::shared_ptr<ApplicationSettings> &appSettings,
        QObject *parent = nullptr);
    ~ServerDataModel();

private:
    CpuStatisticsModel *cpuStatistics()
    {
        return &cpuStatistics_;
    }

    MemoryStatistics *memoryStatistics()
    {
        return &memoryStatistics_;
    }

    ProcessListModel *processList()
    {
        return &processes_;
    }

    DiskInfoModel *diskInfo()
    {
        return &diskInfoModel_;
    }

signals:
    void errorOccured(QString message);

private:
    std::shared_ptr<QNetworkRequest> requestAddress_;
    std::shared_ptr<QNetworkAccessManager> networkAccessManager_;
    std::shared_ptr<ApplicationSettings> appSettings_;

private:
    CpuStatisticsModel cpuStatistics_;
    MemoryStatistics memoryStatistics_;
    ProcessListModel processes_;
    DiskInfoModel diskInfoModel_;
};
