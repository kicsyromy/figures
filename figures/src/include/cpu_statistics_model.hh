#pragma once

#include <memory>

#include "abstract_request_handler.hh"

class CpuStatisticsModel final : public AbstractRequestHandler
{
    Q_OBJECT
    Q_PROPERTY(QList<QObject *> data MEMBER data_ NOTIFY dataChanged)

public:
    CpuStatisticsModel(const std::shared_ptr<ApplicationSettings> &appSettings,
        const std::shared_ptr<QNetworkRequest> &server,
        const std::shared_ptr<QNetworkAccessManager> &networkAccessManager,
        QObject *parent = nullptr);
    ~CpuStatisticsModel();

signals:
    void dataChanged();

private:
    void update(const simdjson::simdjson_result<simdjson::dom::element> &responseJson) override;

private:
    QList<QObject *> data_{};
};
