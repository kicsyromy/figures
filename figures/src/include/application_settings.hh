#pragma once

#include <QObject>

class ApplicationSettings : public QObject
{
    Q_OBJECT
    // clang-format off
    Q_PROPERTY(QString theme             READ theme             WRITE setTheme             NOTIFY themeChanged)
    Q_PROPERTY(QString serverUrl         READ serverUrl         WRITE setServerUrl         NOTIFY serverUrlChanged)
    Q_PROPERTY(QString authenticationKey READ authenticationKey WRITE setAuthenticationKey NOTIFY authenticationKeyChanged)
    Q_PROPERTY(int     refreshInterval   READ refreshInterval   WRITE setRefreshInterval   NOTIFY refreshIntervalChanged)
    // clang-format on

public:
    ApplicationSettings(QObject *parent = nullptr);

public:
    QString theme() const
    {
        return theme_;
    }

    void setTheme(const QString &value);

    QString serverUrl() const
    {
        return serverUrl_;
    }

    void setServerUrl(const QString &value);

    QString authenticationKey() const
    {
        return authenticationKey_;
    }

    void setAuthenticationKey(const QString &value);

    int refreshInterval() const
    {
        return refreshInterval_;
    }

    void setRefreshInterval(int value);

signals:
    void themeChanged();
    void serverUrlChanged();
    void authenticationKeyChanged();
    void refreshIntervalChanged();

private:
    QString theme_;
    QString serverUrl_;
    QString authenticationKey_;
    int refreshInterval_;
};
