#pragma once

#include "abstract_request_handler.hh"
#include "disk_info.hh"

#include <utility>
#include <vector>

#include <QAbstractListModel>

class DiskInfoModel final : public AbstractRequestHandler
{
    Q_OBJECT
    Q_PROPERTY(QAbstractListModel *modelData READ modelData CONSTANT)

public:
    DiskInfoModel(const std::shared_ptr<ApplicationSettings> &appSettings,
        const std::shared_ptr<QNetworkRequest> &server,
        const std::shared_ptr<QNetworkAccessManager> &networkAccessManager,
        QObject *parent = nullptr);
    ~DiskInfoModel();

public:
    QAbstractListModel *modelData();

private:
    void update(const simdjson::simdjson_result<simdjson::dom::element> &responseJson) override;

private:
    class DiskInfoModelData *modelData_;
};

class DiskInfoModelData final : public QAbstractListModel
{
    Q_OBJECT

public:
    enum RoleNames
    {
        DiskInfo = Qt::DisplayRole + 1,
    };

public:
    DiskInfoModelData(QObject *parent = nullptr)
      : QAbstractListModel{ parent }
      , roleNames_({
            std::pair<int, QByteArray>{ RoleNames::DiskInfo, "diskInfo" },
        })
    {}

public:
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

public:
    void update(const simdjson::simdjson_result<simdjson::dom::element> &responseJson,
        QNetworkReply *reply);

signals:
    void errorOccured(QString message);

private:
    QHash<int, QByteArray> roleNames_;

    std::vector<std::pair<bool, std::unique_ptr<class Drive>>> data_{};
    std::vector<std::decay_t<decltype(data_)>::iterator> toRemove_{};
};
