#pragma once

#include <QObject>

#include <QAbstractListModel>

class Filesystem : public QObject
{
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(QList<QString> mountPoints READ mountPoints NOTIFY mountPointsChanged)
    Q_PROPERTY(QString        filesystem  READ filesystem  NOTIFY filesystemChanged)
    Q_PROPERTY(quint64        sizeBytes   READ sizeBytes   NOTIFY sizeBytesChanged)
    Q_PROPERTY(quint64        usedBytes   READ usedBytes   NOTIFY usedBytesChanged)
    // clang-format on

public:
    inline explicit Filesystem(QObject *parent = nullptr)
      : QObject(parent)
    {}

    inline explicit Filesystem(Filesystem &&other)
      : mountPoints_{ std::move(other.mountPoints_) }
      , filesystem_{ std::move(other.filesystem_) }
      , sizeBytes_{ other.sizeBytes_ }
      , usedBytes_{ other.usedBytes_ }
    {
        setParent(other.parent());
    }

    inline Filesystem &operator=(Filesystem &&other)
    {
        mountPoints_ = std::move(other.mountPoints_);
        filesystem_ = std::move(other.filesystem_);
        sizeBytes_ = other.sizeBytes_;
        usedBytes_ = other.usedBytes_;

        setParent(other.parent());

        return *this;
    }

public:
    inline QList<QString> mountPoints() const
    {
        return mountPoints_;
    }

    inline QString filesystem() const
    {
        return filesystem_;
    }

    constexpr quint64 sizeBytes() const
    {
        return sizeBytes_;
    }

    constexpr quint64 usedBytes() const
    {
        return usedBytes_;
    }

signals:
    void mountPointsChanged();
    void filesystemChanged();
    void sizeBytesChanged();
    void usedBytesChanged();

private:
    inline void setMountPoints(const QList<QString> &value)
    {
        if (mountPoints_ != value)
        {
            mountPoints_ = value;
            emit mountPointsChanged();
        }
    }

    inline void setMountPoints(QList<QString> &&value)
    {
        if (mountPoints_ != value)
        {
            mountPoints_ = std::move(value);
            emit mountPointsChanged();
        }
    }

    inline void setFilesystem(const QString &value)
    {
        if (filesystem_ != value)
        {
            filesystem_ = value;
            emit filesystemChanged();
        }
    }

    inline void setFilesystem(QString &&value)
    {
        if (filesystem_ != value)
        {
            filesystem_ = std::move(value);
            emit filesystemChanged();
        }
    }

    inline void setSizeBytes(quint64 value)
    {
        if (sizeBytes_ != value)
        {
            sizeBytes_ = value;
            emit sizeBytesChanged();
        }
    }

    inline void setUsedBytes(quint64 value)
    {
        if (usedBytes_ != value)
        {
            usedBytes_ = value;
            emit usedBytesChanged();
        }
    }

private:
    QList<QString> mountPoints_{};
    QString filesystem_{};
    quint64 sizeBytes_{ 0 };
    quint64 usedBytes_{ 0 };

private:
    friend class DiskInfoModel;
    friend class DiskInfoModelData;
};

class BlockDevice : public QObject
{
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(QString   deviceFile    READ deviceFile    NOTIFY deviceFileChanged)
    Q_PROPERTY(quint64   sizeBytes     READ sizeBytes     NOTIFY sizeBytesChanged)
    Q_PROPERTY(QString   label         READ label         NOTIFY labelChanged)
    Q_PROPERTY(QString   uuid          READ uuid          NOTIFY uuidChanged)
    Q_PROPERTY(float     readSpeedBps  READ readSpeedBps  NOTIFY readSpeedBpsChanged)
    Q_PROPERTY(float     writeSpeedBps READ writeSpeedBps NOTIFY writeSpeedBpsChanged)
    Q_PROPERTY(QObject * filesystem    READ filesystem    CONSTANT)
    // clang-format on

public:
    explicit BlockDevice(QObject *parent = nullptr)
      : QObject(parent)
    {}

    explicit BlockDevice(BlockDevice &&other)
      : deviceFile_{ std::move(other.deviceFile_) }
      , sizeBytes_{ other.sizeBytes_ }
      , label_{ other.label_ }
      , uuid_{ other.uuid_ }
      , readSpeedBps_{ other.readSpeedBps_ }
      , writeSpeedBps_{ other.writeSpeedBps_ }
      , filesystem_{ std::move(other.filesystem_) }
    {
        setParent(other.parent());
    }

    BlockDevice &operator=(BlockDevice &&other)
    {
        deviceFile_ = std::move(other.deviceFile_);
        sizeBytes_ = other.sizeBytes_;
        label_ = other.label_;
        uuid_ = other.uuid_;
        readSpeedBps_ = other.readSpeedBps_;
        writeSpeedBps_ = other.writeSpeedBps_;
        filesystem_ = std::move(other.filesystem_);

        setParent(other.parent());

        return *this;
    }

public:
    inline QString deviceFile() const
    {
        return deviceFile_;
    }

    constexpr quint64 sizeBytes() const
    {
        return sizeBytes_;
    }

    inline QString label() const
    {
        return label_;
    }

    inline QString uuid() const
    {
        return uuid_;
    }

    constexpr float readSpeedBps() const
    {
        return readSpeedBps_;
    }

    inline float writeSpeedBps() const
    {
        return writeSpeedBps_;
    }

    inline QObject *filesystem()
    {
        return &filesystem_;
    }

private:
    void setDeviceFile(QString value)
    {
        if (deviceFile_ != value)
        {
            deviceFile_ = value;
            emit deviceFileChanged();
        }
    }

    void setSizeBytes(quint64 value)
    {
        if (sizeBytes_ != value)
        {
            sizeBytes_ = value;
            emit sizeBytesChanged();
        }
    }

    void setLabel(QString value)
    {
        if (label_ != value)
        {
            label_ = value;
            emit labelChanged();
        }
    }

    void setUuid(QString value)
    {
        if (uuid_ != value)
        {
            uuid_ = value;
            emit uuidChanged();
        }
    }

    void setReadSpeedBps(float value)
    {
        if (readSpeedBps_ != value)
        {
            readSpeedBps_ = value;
            emit readSpeedBpsChanged();
        }
    }

    void setWriteSpeedBps(float value)
    {
        if (writeSpeedBps_ != value)
        {
            writeSpeedBps_ = value;
            emit writeSpeedBpsChanged();
        }
    }

signals:
    void deviceFileChanged();
    void sizeBytesChanged();
    void labelChanged();
    void uuidChanged();
    void readSpeedBpsChanged();
    void writeSpeedBpsChanged();

private:
    QString deviceFile_{};
    quint64 sizeBytes_{};
    QString label_{};
    QString uuid_{};
    float readSpeedBps_{ 0.0f };
    float writeSpeedBps_{ 0.0f };
    Filesystem filesystem_{};

private:
    friend class DiskInfoModel;
    friend class DiskInfoModelData;
    friend class BlockDeviceListModel;
};

class Drive : public QObject
{
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(bool                isPhysicalDrive READ isPhysicalDrive NOTIFY isPhysicalDriveChanged)
    Q_PROPERTY(QString             model           READ model           NOTIFY modelChanged)
    Q_PROPERTY(QString             serialNumber    READ serialNumber    NOTIFY serialNumberChanged)
    Q_PROPERTY(quint64             capacity        READ capacity        NOTIFY capacityChanged)
    Q_PROPERTY(QAbstractListModel* blockDevices    READ blockDevices    NOTIFY blockDevicesChanged)
    Q_PROPERTY(bool                hasSmartInfo    READ hasSmartInfo    NOTIFY hasSmartInfoChanged)
    Q_PROPERTY(float               temperature     READ temperature     NOTIFY temperatureChanged)
    Q_PROPERTY(QString             smartStatus     READ smartStatus     NOTIFY smartStatusChanged)
    Q_PROPERTY(float               readSpeedBps    READ readSpeedBps    NOTIFY readSpeedBpsChanged)
    Q_PROPERTY(float               writeSpeedBps   READ writeSpeedBps   NOTIFY writeSpeedBpsChanged)
    // clang-format on

public:
    explicit Drive(QObject *parent = nullptr);

public:
    inline bool isPhysicalDrive() const
    {
        return isPhysicalDrive_;
    }

    inline QString model() const
    {
        return model_;
    }

    inline QString serialNumber() const
    {
        return serialNumber_;
    }

    constexpr quint64 capacity() const
    {
        return capacity_;
    }

    inline QAbstractListModel *blockDevices()
    {
        return blockDevices_;
    }

    inline bool hasSmartInfo() const
    {
        return hasSmartInfo_;
    }

    inline float temperature() const
    {
        return temperature_;
    }

    inline QString smartStatus() const
    {
        return smartStatus_;
    }

    float readSpeedBps() const;

    float writeSpeedBps() const;

signals:
    void isPhysicalDriveChanged();
    void modelChanged();
    void serialNumberChanged();
    void capacityChanged();
    void blockDevicesChanged();
    void hasSmartInfoChanged();
    void temperatureChanged();
    void smartStatusChanged();
    void readSpeedBpsChanged();
    void writeSpeedBpsChanged();

private:
    void setIsPhysicalDrive(bool value)
    {
        if (isPhysicalDrive_ != value)
        {
            isPhysicalDrive_ = value;
            emit isPhysicalDriveChanged();
        }
    }

    void setModel(const QString &value)
    {
        if (model_ != value)
        {
            model_ = value;
            emit modelChanged();
        }
    }

    void setModel(QString &&value)
    {
        if (model_ != value)
        {
            model_ = std::move(value);
            emit modelChanged();
        }
    }

    void setSerialNumber(const QString &value)
    {
        if (serialNumber_ != value)
        {
            serialNumber_ = value;
            emit serialNumberChanged();
        }
    }

    void setSerialNumber(QString &&value)
    {
        if (serialNumber_ != value)
        {
            serialNumber_ = std::move(value);
            emit serialNumberChanged();
        }
    }

    void setCapacity(quint64 value)
    {
        if (capacity_ != value)
        {
            capacity_ = value;
            emit capacityChanged();
        }
    }

    void setHasSmartInfo(bool value)
    {
        if (hasSmartInfo_ != value)
        {
            hasSmartInfo_ = value;
            emit hasSmartInfoChanged();
        }
    }

    void setTemperature(float value)
    {
        if (temperature_ != value)
        {
            temperature_ = value;
            emit temperatureChanged();
        }
    }

    void setSmartStatus(const QString &value)
    {
        if (smartStatus_ != value)
        {
            smartStatus_ = value;
            emit smartStatusChanged();
        }
    }

    void setSmartStatus(QString &&value)
    {
        if (smartStatus_ != value)
        {
            smartStatus_ = std::move(value);
            emit smartStatusChanged();
        }
    }

private:
    bool isPhysicalDrive_{ false };
    QString model_{};
    QString serialNumber_{};
    quint64 capacity_{ 0 };
    QAbstractListModel *blockDevices_{ nullptr };
    bool hasSmartInfo_{ false };
    float temperature_{ -1 };
    QString smartStatus_{};

private:
    friend class DiskInfoModel;
    friend class DiskInfoModelData;
};
