#pragma once

#undef slots
#include <simdjson/simdjson.h>

#include <memory>
#include <optional>
#include <vector>

#include <QObject>
#include <QTimer>

#define ASSERT_PARSE_FAIL_MSG(message, json_doc)                                                \
    do                                                                                          \
    {                                                                                           \
        if (json_doc.error() != error_code::SUCCESS)                                            \
        {                                                                                       \
            emit errorOccured(QStringLiteral(message "\":") + error_message(json_doc.error())); \
            ongoingRequest_->deleteLater();                                                     \
            ongoingRequest_ = nullptr;                                                          \
            return;                                                                             \
        }                                                                                       \
    } while (false)

#define ASSERT_PARSE_FAIL(key_name, json_doc) \
    ASSERT_PARSE_FAIL_MSG("Failed to parse \"" key_name "\"", json_doc)

class ApplicationSettings;
class QNetworkRequest;
class QNetworkAccessManager;
class QNetworkReply;

class AbstractRequestHandler : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool refreshing READ refreshing WRITE setRefreshing NOTIFY refreshingChanged)

public:
    enum class RequestType
    {
        CpuUsage,
        MemoryUsage,
        ProcessList,
        DiskInfo,
    };

public:
    AbstractRequestHandler(const std::shared_ptr<ApplicationSettings> &appSettings,
        const std::shared_ptr<QNetworkRequest> &server,
        const std::shared_ptr<QNetworkAccessManager> &networkAccessManager,
        RequestType requestType,
        std::vector<std::string> &&parameters,
        QObject *parent = nullptr);
    virtual ~AbstractRequestHandler();

public:
    bool refreshing() const
    {
        return refreshing_;
    }

    void setRefreshing(bool value)
    {
        if (refreshing_ != value)
        {
            refreshing_ = value;

            if (value)
            {
                refreshTimer_.start();
            }
            else
            {
                refreshTimer_.stop();
            }

            emit refreshingChanged();
        }
    }

signals:
    void errorOccured(QString message);
    void refreshingChanged();

protected:
    virtual void update(const simdjson::simdjson_result<simdjson::dom::element> &responseJson) = 0;

private:
    void makeRequest();
    void onRequestFinished();

protected:
    QNetworkReply *ongoingRequest_;

private:
    std::shared_ptr<QNetworkRequest> server_;
    std::shared_ptr<QNetworkAccessManager> networkAccessManager_;
    std::shared_ptr<ApplicationSettings> appSettings_;

    bool refreshing_{ false };
    bool wasRefreshing_{ false };
    QTimer refreshTimer_;

    simdjson::dom::parser jsonParser_;

private:
    RequestType requestType_;
    QByteArray requestString_;
    std::vector<std::string> requestParameters_;
};
