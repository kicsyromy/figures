#pragma once

#include "disk_info.hh"

#include <memory>
#include <utility>
#include <vector>

#include <QAbstractListModel>

class BlockDeviceListModel final : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString deviceFile READ deviceFile CONSTANT)

public:
    enum class RoleNames
    {
        BlockDevice = Qt::DisplayRole + 1,
    };

    BlockDeviceListModel(QObject *parent = nullptr)
      : QAbstractListModel{ parent }
      , roleNames_({
            std::pair<int, QByteArray>{ int(RoleNames::BlockDevice), "blockDevice" },
        })
      , isPhysicalDrive_{ true }
    {}

public:
    QString deviceFile() const
    {
        return (isPhysicalDrive_ && !data_.empty() ? data_[0].second->deviceFile() : "");
    }

public:
    inline const std::vector<std::pair<bool, std::unique_ptr<BlockDevice>>> &devices() const
    {
        return data_;
    }

    void setIsPhysicalDrive(bool value)
    {
        if (isPhysicalDrive_ != value)
        {
            if (!value)
            {
                beginRemoveRows(QModelIndex{}, 0, 0);
                endRemoveRows();
            }

            isPhysicalDrive_ = value;
        }
    }

private:
    void addOrUpdate(class BlockDevice &&blockDevice);

public:
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

private:
    QHash<int, QByteArray> roleNames_;
    bool isPhysicalDrive_;

    std::vector<std::pair<bool, std::unique_ptr<BlockDevice>>> data_{};
    std::vector<std::decay_t<decltype(data_)>::iterator> toRemove_{};

private:
    friend class DiskInfoModel;
    friend class DiskInfoModelData;
};
