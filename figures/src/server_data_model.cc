#include "server_data_model.hh"
#include "cpu_statistics_model.hh"

#include <QGuiApplication>
#include <QString>

ServerDataModel::ServerDataModel([[maybe_unused]] QGuiApplication *app,
    const std::shared_ptr<ApplicationSettings> &appSettings,
    QObject *parent)
  : QObject(parent)
  , requestAddress_{ new QNetworkRequest{ QUrl{ appSettings->serverUrl() } } }
  , networkAccessManager_{ new QNetworkAccessManager{} }
  , appSettings_{ appSettings }
  , cpuStatistics_{ appSettings, requestAddress_, networkAccessManager_ }
  , memoryStatistics_{ appSettings, requestAddress_, networkAccessManager_ }
  , processes_{ appSettings, requestAddress_, networkAccessManager_ }
  , diskInfoModel_{ appSettings, requestAddress_, networkAccessManager_ }
{
#ifdef Q_OS_ANDROID
    QObject::connect(app,
        &QGuiApplication::applicationStateChanged,
        this,
        [this](Qt::ApplicationState state) {
            switch (state)
            {
            case Qt::ApplicationState::ApplicationHidden:
            case Qt::ApplicationState::ApplicationInactive:
            case Qt::ApplicationState::ApplicationSuspended:
                cpuStatistics_.setRefreshing(false);
                memoryStatistics_.setRefreshing(false);
                processes_.setRefreshing(false);
                break;
            case Qt::ApplicationState::ApplicationActive:
                break;
            }
        });
#endif

    QObject::connect(appSettings.get(), &ApplicationSettings::serverUrlChanged, this, [this] {
        requestAddress_->setUrl(QUrl{ appSettings_->serverUrl() });
    });

    QObject::connect(&cpuStatistics_,
        &CpuStatisticsModel::errorOccured,
        this,
        &ServerDataModel::errorOccured);

    QObject::connect(&memoryStatistics_,
        &MemoryStatistics::errorOccured,
        this,
        &ServerDataModel::errorOccured);

    QObject::connect(&processes_,
        &ProcessListModel::errorOccured,
        this,
        &ServerDataModel::errorOccured);

    QObject::connect(&diskInfoModel_,
        &DiskInfoModel::errorOccured,
        this,
        &ServerDataModel::errorOccured);

    requestAddress_->setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader,
        "application/json");
}

ServerDataModel::~ServerDataModel() = default;
