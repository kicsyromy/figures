#include "application_settings.hh"
#include "server_data_model.hh"
#include "settings_model.hh"

#include <QFont>
#include <QFontDatabase>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>
#include <QQuickWindow>

//#ifdef Q_OS_ANDROID
//#include <QtAndroidExtras>
//#endif

int main(int argc, char *argv[])
{
    QQuickStyle::setStyle("Material");

    QCoreApplication::setOrganizationName("kicsyromy");
    QCoreApplication::setOrganizationDomain("kicsyromy.me");
    QCoreApplication::setApplicationName("Figures");

    //#ifdef Q_OS_ANDROID
    //    QtAndroid::runOnAndroidThread([=]() {
    //        QAndroidJniObject window =
    //            QtAndroid::androidActivity().callObjectMethod("getWindow",
    //            "()Landroid/view/Window;");
    //        window.callMethod<void>("addFlags", "(I)V", 0x80000000);
    //        window.callMethod<void>("clearFlags", "(I)V", 0x04000000);
    //        window.callMethod<void>("setStatusBarColor", "(I)V", 0xFF1C1C1C); // Desired statusbar
    //        color
    //    });
    //#endif

    QGuiApplication app(argc, argv);

    qmlRegisterUncreatableType<SettingsSections>("Figures",
        1,
        0,
        "settingsSections",
        "Not creatable as it is an enum type");

    QQmlApplicationEngine engine;

    auto appSettings = std::make_shared<ApplicationSettings>();
    engine.rootContext()->setContextProperty("ApplicationSettings", appSettings.get());

    engine.rootContext()->setContextProperty("ServerDataModel",
        new ServerDataModel(&app, appSettings, engine.rootContext()));

    engine.rootContext()->setContextProperty("SettingsModel",
        new SettingsModel(engine.rootContext()));

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreated,
        &app,
        [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        },
        Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
