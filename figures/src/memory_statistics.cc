#include <simdjson/simdjson.h>

#include "memory_statistics.hh"

#include <QNetworkReply>

MemoryStatistics::MemoryStatistics(const std::shared_ptr<ApplicationSettings> &appSettings,
    const std::shared_ptr<QNetworkRequest> &server,
    const std::shared_ptr<QNetworkAccessManager> &networkAccessManager,
    QObject *parent)
  : AbstractRequestHandler{ appSettings,
      server,
      networkAccessManager,
      AbstractRequestHandler::RequestType::MemoryUsage,
      {},
      parent }
{}

MemoryStatistics::~MemoryStatistics() = default;

void MemoryStatistics::update(const simdjson::simdjson_result<simdjson::dom::element> &responseJson)
{
    using namespace simdjson;

    auto memStats = responseJson.get_object();
    ASSERT_PARSE_FAIL("response(memory_usage)", memStats);

    const auto total = memStats.at_key("total").get_int64();
    ASSERT_PARSE_FAIL("total", total);
    if (total.first != bytesTotal_)
    {
        bytesTotal_ = total.first;
        emit bytesTotalChanged();
    }

    const auto used = memStats.at_key("used").get_int64();
    ASSERT_PARSE_FAIL("used", used);
    if (used.first != bytesUsed_)
    {
        bytesUsed_ = used.first;
        emit bytesUsedChanged();
    }

    const auto swapTotal = memStats.at_key("swap_total").get_int64();
    ASSERT_PARSE_FAIL("swap_total", swapTotal);
    if (swapTotal.first != swapBytesTotal_)
    {
        swapBytesTotal_ = swapTotal.first;
        emit swapBytesTotalChanged();
    }

    const auto swapUsed = memStats.at_key("swap_used").get_int64();
    ASSERT_PARSE_FAIL("swap_used", swapUsed);
    if (swapUsed.first != swapBytesUsed_)
    {
        swapBytesUsed_ = swapUsed.first;
        emit swapBytesUsedChanged();
    }
}
