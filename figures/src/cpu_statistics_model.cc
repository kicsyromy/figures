#include <simdjson/simdjson.h>

#include "cpu_statistics.hh"
#include "cpu_statistics_model.hh"

#include <QNetworkReply>
#include <QString>

CpuStatisticsModel::CpuStatisticsModel(const std::shared_ptr<ApplicationSettings> &appSettings,
    const std::shared_ptr<QNetworkRequest> &server,
    const std::shared_ptr<QNetworkAccessManager> &networkAccessManager,
    QObject *parent)
  : AbstractRequestHandler{ appSettings,
      server,
      networkAccessManager,
      AbstractRequestHandler::RequestType::CpuUsage,
      {},
      parent }
  , data_{}
{}

CpuStatisticsModel::~CpuStatisticsModel() = default;

void CpuStatisticsModel::update(
    const simdjson::simdjson_result<simdjson::dom::element> &responseJson)
{
    using namespace simdjson;

    auto cpuStats = responseJson.get_array();
    ASSERT_PARSE_FAIL("response(cpu_usage)", cpuStats);

    bool cpuStatisticsListUpdated = false;

    if (!data_.empty() && cpuStats.size() != static_cast<std::size_t>(data_.length()))
    {
        qWarning() << "CPU core count changed... ahm... OK... clearing stats and staring fresh";
        qDeleteAll(data_);
        data_.clear();
    }

    if (data_.empty())
    {
        data_.reserve(cpuStats.size());
        cpuStatisticsListUpdated = true;
    }

    for (const auto &stat : cpuStats)
    {
        auto processor = stat.at_key("processor").get_string();
        ASSERT_PARSE_FAIL("processor", processor);

        auto physicalId = stat.at_key("physical_id").get_int64();
        ASSERT_PARSE_FAIL("physical_id", physicalId);

        auto logicalId = stat.at_key("logical_id").get_int64();
        ASSERT_PARSE_FAIL("logical_id", logicalId);

        auto activePercent = stat.at_key("active_percent").get_double();
        ASSERT_PARSE_FAIL("active_percent", activePercent);

        auto idlePercent = stat.at_key("idle_percent").get_double();
        ASSERT_PARSE_FAIL("idle_percent", idlePercent);

        auto frequency = stat.at_key("frequency").get_double();
        ASSERT_PARSE_FAIL("frequency", frequency);

        auto temperatureCurrent = stat.at_key("temperature_current").get_double();
        ASSERT_PARSE_FAIL("temperature_current", temperatureCurrent);

        auto temperatureThreshold = stat.at_key("temperature_threshold").get_double();
        ASSERT_PARSE_FAIL("temperature_threshold", temperatureThreshold);

        auto it = std::find_if(data_.begin(), data_.end(), [&logicalId](QObject *cpuStat) {
            return static_cast<CpuStatistics *>(cpuStat)->logicalId() == logicalId.value();
        });

        if (it != data_.end())
        {
            auto cpuStat = static_cast<CpuStatistics *>(*it);
            cpuStat->setActivePercent(activePercent.value());
            cpuStat->setIdlePercent(idlePercent.value());
            cpuStat->setFrequency(frequency.value());
            cpuStat->setTemperatureCurrent(temperatureCurrent.value());
            cpuStat->setTemperatureThreshold(temperatureThreshold.value());
        }
        else
        {
            auto cpuStat = new CpuStatistics(this);
            cpuStat->setProcessor(
                QString::fromLocal8Bit(processor.value().data(), processor.value().size()));
            cpuStat->setPhysicalId(physicalId.value());
            cpuStat->setLogicalId(logicalId.value());
            cpuStat->setActivePercent(activePercent.value());
            cpuStat->setIdlePercent(idlePercent.value());
            cpuStat->setFrequency(frequency.value());
            cpuStat->setTemperatureCurrent(temperatureCurrent.value());
            cpuStat->setTemperatureThreshold(temperatureThreshold.value());

            data_.append(cpuStat);
        }
    }

    ongoingRequest_->deleteLater();
    ongoingRequest_ = nullptr;

    if (cpuStatisticsListUpdated)
    {
        emit dataChanged();
    }
}
