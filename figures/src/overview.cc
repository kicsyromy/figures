#include <simdjson/simdjson.h>

#include "overview.hh"

#include <QNetworkReply>

Overview::Overview(const std::shared_ptr<ApplicationSettings> &appSettings,
    const std::shared_ptr<QNetworkRequest> &server,
    const std::shared_ptr<QNetworkAccessManager> &networkAccessManager,
    QObject *parent)
  : AbstractRequestHandler{ appSettings,
      server,
      networkAccessManager,
      AbstractRequestHandler::RequestType::MemoryUsage,
      {},
      parent }
{}

Overview::~Overview() = default;

void Overview::update(const simdjson::simdjson_result<simdjson::dom::element> &responseJson)
{}
