#include "block_device_model.hh"

void BlockDeviceListModel::addOrUpdate(BlockDevice &&blockDevice)
{
    auto it = std::find_if(data_.begin(), data_.end(), [&blockDevice](const auto &current) {
        return current.second->deviceFile() == blockDevice.deviceFile();
    });

    if (it != data_.end())
    {
        auto &bd = *it;
        bd.first = true;
        bd.second->setReadSpeedBps(blockDevice.readSpeedBps());
        bd.second->setWriteSpeedBps(blockDevice.writeSpeedBps());
        bd.second->filesystem_ = std::move(blockDevice.filesystem_);
    }
    else
    {
        auto newPosition =
            std::find_if(data_.begin(), data_.end(), [&blockDevice](const auto &current) {
                return current.second->deviceFile() > blockDevice.deviceFile();
            });

        auto bd = std::make_unique<BlockDevice>(std::move(blockDevice));

        const auto newIndex = static_cast<int>(newPosition - data_.begin());
        beginInsertRows(QModelIndex{}, newIndex, newIndex);
        {
            data_.insert(newPosition, { true, std::move(bd) });
        }
        endInsertRows();
    }
}

int BlockDeviceListModel::rowCount([[maybe_unused]] const QModelIndex &parent) const
{
    return static_cast<int>(isPhysicalDrive_ && !data_.empty() ? data_.size() - 1 : data_.size());
}

QVariant BlockDeviceListModel::data(const QModelIndex &i, int r) const
{
    const auto index = static_cast<std::size_t>(i.row());
    const auto role = static_cast<RoleNames>(r);

    switch (role)
    {
    case RoleNames::BlockDevice:
        return QVariant::fromValue(
            static_cast<QObject *>(data_[isPhysicalDrive_ ? index + 1 : index].second.get()));
    }

    return QVariant();
}

QHash<int, QByteArray> BlockDeviceListModel::roleNames() const
{
    return roleNames_;
}
