QT += core quick network svg quickcontrols2
#unix:android {
#    QT += androidextras
#}

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

include(3rdparty/3rdparty.pri)

HEADERS += \
    src/include/abstract_request_handler.hh \
    src/include/application_settings.hh \
    src/include/block_device_model.hh \
    src/include/cpu_statistics.hh \
    src/include/cpu_statistics_model.hh \
    src/include/disk_info.hh \
    src/include/disk_info_model.hh \
    src/include/memory_statistics.hh \
    src/include/overview.hh \
    src/include/process.hh \
    src/include/process_list_model.hh \
    src/include/server_data_model.hh \
    src/include/settings_model.hh

SOURCES += \
    src/abstract_request_handler.cc \
    src/application_settings.cc \
    src/block_device_model.cc \
    src/cpu_statistics_model.cc \
    src/disk_info.cc \
    src/disk_info_model.cc \
    src/main.cc \
    src/memory_statistics.cc \
    src/server_data_model.cc \
    src/overview.cc \
    src/process_list_model.cc \
    src/settings_model.cc

INCLUDEPATH += src/include

RESOURCES += src/resources/resources.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle.properties \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
