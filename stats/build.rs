use std::env;
use std::path::{Path, PathBuf};

use std::error::Error;
use std::fs::File;
use std::io::Write;

fn main() -> Result<(), Box<dyn Error>> {
    println!("cargo:rustc-link-lib=dylib=sensors");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    generate_lm_sensors_bindings(out_path.as_path())?;
    generate_statvfs_bindings(out_path.as_path())?;
    generate_udisks2_bindings(out_path.as_path())?;

    Ok(())
}

fn generate_lm_sensors_bindings(out_path: &Path) -> Result<(), Box<dyn Error>> {
    let temp_file_path = out_path
        .to_str()
        .ok_or("Failed to create temporary file")?
        .to_string()
        + "/lmsensors.h";
    let mut temp_file = File::create(temp_file_path.as_str())?;
    temp_file.write("#include <sensors/sensors.h>\n#include <sensors/error.h>".as_bytes())?;

    let bindings = bindgen::Builder::default()
        .header(temp_file_path.as_str())
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .whitelist_type("^sensors.*")
        .whitelist_var("^SENSORS.*")
        .whitelist_var("^sensors.*")
        .whitelist_function("^sensors.*")
        .layout_tests(false)
        .generate()
        .ok()
        .ok_or("Failed to generate bindings for lm-sensors")?;

    bindings
        .write_to_file(out_path.join("sensors-sys.rs"))
        .map_err(|e| e.into())
}

fn generate_statvfs_bindings(out_path: &Path) -> Result<(), Box<dyn Error>> {
    let temp_file_path = out_path
        .to_str()
        .ok_or("Failed to create temporary file")?
        .to_string()
        + "/vfsstats.h";
    let mut temp_file = File::create(temp_file_path.as_str())?;
    temp_file.write("#include <sys/statvfs.h>".as_bytes())?;

    let bindings = bindgen::Builder::default()
        .header(temp_file_path.as_str())
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .whitelist_type("^statvfs.*")
        .whitelist_var("^$")
        .whitelist_function("^fstat.*")
        .whitelist_function("^stat.*")
        .layout_tests(false)
        .generate()
        .ok()
        .ok_or("Failed to generate bindings for statvfs")?;

    bindings
        .write_to_file(out_path.join("statvfs-sys.rs"))
        .map_err(|e| e.into())
}

fn generate_udisks2_bindings(out_path: &Path) -> Result<(), Box<dyn Error>> {
    let udisks2_lib = pkg_config::Config::new()
        .atleast_version("2.8.0")
        .probe("udisks2")?;

    let mut include_args = Vec::new();
    for path in &udisks2_lib.include_paths {
        include_args.push("-I".to_string() + path.to_str().unwrap());
    }

    let temp_file_path = out_path
        .to_str()
        .ok_or("Failed to create temporary file")?
        .to_string()
        + "/udisks2.h";
    let mut temp_file = File::create(temp_file_path.as_str())?;
    temp_file.write("#include <udisks/udisks.h>".as_bytes())?;

    let bindings = bindgen::Builder::default()
        .header(temp_file_path.as_str())
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .whitelist_type("^UDisks.*")
        .whitelist_var("^UDISKS.*")
        .whitelist_function("^udisks_.*")
        .whitelist_function("^g_variant.*")
        .whitelist_function("^g_free.*")
        .whitelist_function("^g_object_unref.*")
        .whitelist_function("^g_error.*")
        .whitelist_function("^g_object_get.*")
        .clang_args(include_args.iter())
        .layout_tests(false)
        .generate()
        .ok()
        .ok_or("Failed to generate bindings for udisks2")?;

    bindings
        .write_to_file(out_path.join("udisks2-sys.rs"))
        .map_err(|e| e.into())
}
