mod constants;
mod cpu_usage_handler;
mod disk_info_handler;
mod error_handler;
mod memory_usage_handler;
mod process_list_handler;
mod request;
mod response;
mod system_overview_handler;

pub mod service;

pub async fn run(address: [u8; 4], port: u16) -> Result<(), Box<dyn std::error::Error>> {
    use hyper::service::{make_service_fn, service_fn};
    use hyper::Server;
    use std::convert::Infallible;
    use std::convert::TryFrom;
    use std::net::SocketAddr;

    let address = SocketAddr::from((address, port));
    let server = Server::bind(&address)
        .serve(make_service_fn(|_conn| async {
            Ok::<_, Infallible>(service_fn(service::handle_request))
        }))
        .with_graceful_shutdown(shutdown_signal());

    if let Err(e) = server.await {
        Err(Box::try_from(e).unwrap())
    } else {
        Ok(())
    }
}

async fn shutdown_signal() {
    // Wait for the CTRL+C signal
    tokio::signal::ctrl_c()
        .await
        .expect("failed to install CTRL+C signal handler");
}
