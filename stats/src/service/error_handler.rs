pub fn handle_bad_request(method: &str, message: &str) -> hyper::Response<hyper::Body> {
    use crate::service::response;
    use hyper::{Body, Response, StatusCode};

    let response = response::Response {
        method,
        response: (),
        error: message,
    };

    let response = serde_json::to_string(&response).unwrap();

    let mut bad_request = Response::new(Body::from(response));
    *bad_request.status_mut() = StatusCode::BAD_REQUEST;
    bad_request
        .headers_mut()
        .insert("Content-Type", "application/json".parse().unwrap());
    return bad_request;
}

pub fn handle_internal_error(method: &str, message: &str) -> hyper::Response<hyper::Body> {
    use crate::service::response;
    use hyper::{Body, Response, StatusCode};

    let response = response::Response {
        method,
        response: (),
        error: message,
    };

    let response = serde_json::to_string(&response).unwrap();

    let mut internal_error = Response::new(Body::from(response));
    *internal_error.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
    internal_error
        .headers_mut()
        .insert("Content-Type", "application/json".parse().unwrap());
    return internal_error;
}
