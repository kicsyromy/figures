pub const METHOD_CPU_USAGE: &'static str = "cpu_usage";
pub const METHOD_MEMORY_USAGE: &'static str = "memory_usage";
pub const METHOD_PROCESS_LIST: &'static str = "process_list";
pub const METHOD_DISK_INFO: &'static str = "disk_info";
pub const METHOD_SYSTEM_OVERVIEW: &'static str = "system_overview";
