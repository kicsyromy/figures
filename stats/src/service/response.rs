use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct Response<'a, T> {
    pub method: &'a str,
    pub response: T,
    pub error: &'a str,
}
