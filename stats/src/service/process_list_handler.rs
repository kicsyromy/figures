use crate::service::constants::*;
use crate::systeminfo::process_list::Process;
use serde::Serialize;

#[derive(Debug, Serialize)]
struct Response {
    pub core_count: u64,
    pub total_memory: u64,
    pub process_list: Vec<Process>,
}

struct Parameter {
    pub index: usize,
    pub value: String,
}

const VALID_PARAMETERS: [&str; 1] = ["filter"];
const PARAMETER_INDEX_FILTER: usize = 0;

fn extract_parameters(parameters: &Vec<String>) -> Result<Vec<Parameter>, String> {
    let mut result = Vec::new();

    for param in parameters.iter() {
        let key_value = param
            .split('=')
            .map(|string| string.trim())
            .collect::<Vec<&str>>();

        if key_value.len() != 2 {
            return Err(format!("Cannot parse parameter '{}'", param));
        }

        let parameter_name = VALID_PARAMETERS
            .iter()
            .enumerate()
            .find(|(_, s)| **s == key_value[0]);
        if parameter_name.is_none() {
            return Err(format!("Unknown parameter '{}'", key_value[0]));
        }

        result.push(Parameter {
            index: parameter_name.unwrap().0,
            value: key_value[1].to_string(),
        })
    }

    Ok(result)
}

pub fn handle_process_list(parameters: &Vec<String>) -> hyper::Response<hyper::Body> {
    use crate::service::error_handler::{handle_bad_request, handle_internal_error};
    use crate::service::response;
    use crate::systeminfo::{cpu_info, memory_statistics, process_list};
    use hyper::{Body, Response};

    let parameters = extract_parameters(parameters);
    if parameters.is_err() {
        return handle_bad_request(METHOD_PROCESS_LIST, parameters.err().unwrap().as_str());
    }

    let mut filters = None;
    let parameters = parameters.unwrap();
    for parameter in &parameters {
        match parameter.index {
            PARAMETER_INDEX_FILTER => {
                let filter_iter = parameter.value.split(';').map(|x| x.trim());
                for filter in filter_iter {
                    let original_filter = filter;
                    let (negate, filter) = if filter.starts_with("?!") {
                        (true, &filter[2..])
                    } else {
                        (false, filter)
                    };

                    let regex = regex::Regex::new(filter);
                    if regex.is_err() {
                        return handle_bad_request(
                            METHOD_PROCESS_LIST,
                            format!(
                                "Filter {} is not a valid regular expression",
                                original_filter
                            )
                            .as_str(),
                        );
                    } else {
                        if (&filters).is_none() {
                            filters = Some(Vec::new());
                        }
                        filters.as_mut().unwrap().push((negate, regex.unwrap()));
                    }
                }
            }
            _ => {}
        }
    }

    let response;

    let filters_slice = if filters.is_some() {
        filters.as_mut().unwrap().sort_unstable_by(|a, b| {
            use std::cmp::Ordering;
            if a.0 {
                if b.0 {
                    Ordering::Equal
                } else {
                    Ordering::Greater
                }
            } else {
                Ordering::Less
            }
        });
        Some(&(filters.as_ref().unwrap()[..]))
    } else {
        None
    };
    let process_list = process_list::Process::gather(filters_slice);
    let core_count = cpu_info::CPUInfo::core_count();
    let mem_info = memory_statistics::Statistics::gather();

    if process_list.is_err() {
        return handle_internal_error(METHOD_PROCESS_LIST, process_list.unwrap_err().as_str());
    } else if core_count.is_err() {
        return handle_internal_error(
            METHOD_PROCESS_LIST,
            core_count.unwrap_err().to_string().as_str(),
        );
    } else if mem_info.is_err() {
        return handle_internal_error(
            METHOD_PROCESS_LIST,
            mem_info.unwrap_err().to_string().as_str(),
        );
    } else {
        use crate::service::process_list_handler::Response as ProcessListResponse;

        response = response::Response {
            method: METHOD_PROCESS_LIST,
            response: ProcessListResponse {
                core_count: core_count.unwrap(),
                total_memory: mem_info.unwrap().total,
                process_list: process_list.unwrap(),
            },
            error: "",
        };
    }

    let response_body = serde_json::to_string(&response).unwrap();
    let response = Response::new(Body::from(response_body));

    response
}
