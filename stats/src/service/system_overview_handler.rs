use crate::service::constants::*;
use crate::systeminfo::{process_list, storage_info};

use serde::Serialize;

#[derive(Debug, Serialize)]
struct OverviewResponse {
    pub cpu_name: String,
    pub cpu_active_percent: f32,
    pub cpu_frequency: f32,
    pub cpu_temperature: f64,
    pub mem_total_bytes: u64,
    pub mem_used_bytes: u64,
    pub filesystems: Vec<storage_info::Filesystem>,
    pub processes_top_cpu_usage: Vec<process_list::Process>,
    pub processes_top_mem_usage: Vec<process_list::Process>,
}

pub fn handle_system_overview(_parameters: &Vec<String>) -> hyper::Response<hyper::Body> {
    use crate::service::error_handler::handle_internal_error;
    use crate::service::response;
    use crate::systeminfo::{cpu_statistics, memory_statistics};

    use hyper::{Body, Response};

    let (cpu_usage_stats, mem_stats, storage_info, process_list) = (
        cpu_statistics::Statistics::gather(cpu_statistics::GatherOptions::UsageAverage),
        memory_statistics::Statistics::gather(),
        storage_info::gather(),
        process_list::Process::gather(None),
    );

    if cpu_usage_stats.is_err() {
        return handle_internal_error(
            METHOD_SYSTEM_OVERVIEW,
            cpu_usage_stats.err().unwrap().as_str(),
        );
    }

    if mem_stats.is_err() {
        return handle_internal_error(METHOD_SYSTEM_OVERVIEW, mem_stats.err().unwrap().as_str());
    }

    if storage_info.is_err() {
        return handle_internal_error(METHOD_SYSTEM_OVERVIEW, storage_info.err().unwrap().as_str());
    }

    if process_list.is_err() {
        return handle_internal_error(METHOD_SYSTEM_OVERVIEW, process_list.err().unwrap().as_str());
    }

    let (cpu_usage_stats, mem_stats, storage_info, mut process_list) = (
        cpu_usage_stats.unwrap(),
        mem_stats.unwrap(),
        storage_info.unwrap(),
        process_list.unwrap(),
    );

    let mut overview_response = OverviewResponse {
        cpu_name: cpu_usage_stats[0].processor.clone(),
        cpu_active_percent: cpu_usage_stats[0].active_percent,
        cpu_frequency: cpu_usage_stats[0].frequency,
        cpu_temperature: cpu_usage_stats[0].temperature_current,
        mem_total_bytes: mem_stats.total,
        mem_used_bytes: mem_stats.used,
        filesystems: vec![],
        processes_top_cpu_usage: vec![],
        processes_top_mem_usage: vec![],
    };

    for drive in &storage_info {
        for block_device in &drive.block_devices {
            if block_device.filesystem.is_none() {
                continue;
            }

            let filesystem = block_device.filesystem.as_ref().unwrap();
            if filesystem.mount_points.is_empty() {
                continue;
            }

            overview_response.filesystems.push(filesystem.clone());
        }
    }

    process_list.sort_unstable_by(|p1, p2| {
        use std::cmp::Ordering;

        let order = p2.cpu_usage_percent.partial_cmp(&p1.cpu_usage_percent);
        return if order.is_none() {
            Ordering::Equal
        } else {
            order.unwrap()
        };
    });
    overview_response.processes_top_cpu_usage = process_list[..10].to_vec();

    process_list.sort_unstable_by(|p1, p2| p2.memory_rss.cmp(&p1.memory_rss));
    overview_response.processes_top_mem_usage = process_list[..10].to_vec();

    let response = response::Response {
        method: METHOD_SYSTEM_OVERVIEW,
        response: overview_response,
        error: "",
    };

    let response_body = serde_json::to_string(&response).unwrap();
    let response = Response::new(Body::from(response_body));

    response
}
