use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Request {
    pub key: String,
    pub method: String,
    pub parameters: Vec<String>,
}

impl Request {
    pub async fn from_body(body: &mut hyper::Body) -> Result<Request, String> {
        let body_content = hyper::body::to_bytes(body)
            .await
            .map_err(|e| e.to_string())?;
        serde_json::from_slice::<Request>(body_content.as_ref()).map_err(|e| e.to_string())
    }
}
