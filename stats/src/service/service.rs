pub async fn handle_request(
    request: hyper::Request<hyper::Body>,
) -> Result<hyper::Response<hyper::Body>, hyper::Error> {
    use crate::service::constants::*;
    use crate::service::cpu_usage_handler::handle_cpu_usage;
    use crate::service::disk_info_handler::handle_disk_info;
    use crate::service::error_handler::handle_bad_request;
    use crate::service::memory_usage_handler::handle_memory_usage;
    use crate::service::process_list_handler::handle_process_list;
    use crate::service::system_overview_handler::handle_system_overview;
    use hyper::{Method, Response, StatusCode};

    if request.method() == &Method::POST && request.uri().path() == "/" {
        let content_size = match request.headers().get("content-length") {
            None => -1,
            Some(value) => value.to_str().unwrap().parse().unwrap(),
        };

        return if content_size != -1 {
            let body = crate::service::request::Request::from_body(&mut request.into_body()).await;
            if body.is_err() {
                return Ok(handle_bad_request(
                    "",
                    format!("Invalid request or JSON body: {}", body.err().unwrap()).as_str(),
                ));
            }

            let body = body.unwrap();
            match body.method.to_lowercase().as_str() {
                METHOD_SYSTEM_OVERVIEW => {
                    let mut response = handle_system_overview(&body.parameters);
                    response
                        .headers_mut()
                        .insert("Content-Type", "application/json".parse().unwrap());
                    Ok(response)
                }
                METHOD_CPU_USAGE => {
                    let mut response = handle_cpu_usage(&body.parameters);
                    response
                        .headers_mut()
                        .insert("Content-Type", "application/json".parse().unwrap());
                    Ok(response)
                }
                METHOD_MEMORY_USAGE => {
                    let mut response = handle_memory_usage(&body.parameters);
                    response
                        .headers_mut()
                        .insert("Content-Type", "application/json".parse().unwrap());
                    Ok(response)
                }
                METHOD_PROCESS_LIST => {
                    let mut response = handle_process_list(&body.parameters);
                    response
                        .headers_mut()
                        .insert("Content-Type", "application/json".parse().unwrap());
                    Ok(response)
                }
                METHOD_DISK_INFO => {
                    let mut response = handle_disk_info(&body.parameters);
                    response
                        .headers_mut()
                        .insert("Content-Type", "application/json".parse().unwrap());
                    Ok(response)
                }
                _ => Ok(handle_bad_request(body.method.as_str(), "Unknown method")),
            }
        } else {
            Ok(handle_bad_request(
                "",
                "Missing or invalid Content-Length header",
            ))
        };
    } else {
        let mut not_found = Response::default();
        *not_found.status_mut() = StatusCode::NOT_FOUND;
        Ok(not_found)
    }
}
