use crate::service::constants::*;

pub fn handle_memory_usage(_parameters: &Vec<String>) -> hyper::Response<hyper::Body> {
    use crate::service::error_handler::handle_internal_error;
    use crate::service::response;
    use crate::systeminfo::memory_statistics;
    use hyper::{Body, Response};

    let response;

    let mem_stats = memory_statistics::Statistics::gather();
    if mem_stats.is_err() {
        return handle_internal_error(METHOD_MEMORY_USAGE, mem_stats.err().unwrap().as_str());
    } else {
        response = response::Response {
            method: METHOD_MEMORY_USAGE,
            response: mem_stats.unwrap(),
            error: "",
        };
    }

    let response_body = serde_json::to_string(&response).unwrap();
    let response = Response::new(Body::from(response_body));

    response
}
