use crate::service::constants::*;

pub fn handle_disk_info(_parameters: &Vec<String>) -> hyper::Response<hyper::Body> {
    use crate::service::error_handler::handle_internal_error;
    use crate::service::response;
    use crate::systeminfo::storage_info;
    use hyper::{Body, Response};

    let response;

    let disk_info = storage_info::gather();
    if disk_info.is_err() {
        return handle_internal_error(METHOD_DISK_INFO, disk_info.err().unwrap().as_str());
    } else {
        response = response::Response {
            method: METHOD_DISK_INFO,
            response: disk_info.unwrap(),
            error: "",
        };
    }

    let response_body = serde_json::to_string(&response).unwrap();
    let response = Response::new(Body::from(response_body));

    response
}
