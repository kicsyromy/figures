use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct Statistics {
    pub total: u64,
    pub free: u64,
    pub used: u64,
    pub swap_total: u64,
    pub swap_free: u64,
    pub swap_used: u64,
}

impl Statistics {
    fn new() -> Self {
        Statistics {
            total: 0,
            free: 0,
            used: 0,
            swap_total: 0,
            swap_free: 0,
            swap_used: 0,
        }
    }

    pub fn gather() -> Result<Self, String> {
        use crate::systeminfo::utilities::parse_sized_field;
        use std::fs::OpenOptions;
        use std::io::{BufRead, BufReader};

        let mut result = Self::new();

        let f = OpenOptions::new()
            .read(true)
            .open("/proc/meminfo")
            .map_err(|e| e.to_string())?;
        let mut freq_reader = BufReader::new(f);

        loop {
            let mut line = String::new();
            let size = freq_reader
                .read_line(&mut line)
                .map_err(|e| e.to_string())?;

            if size == 0 {
                break;
            }

            if line.is_empty() {
                continue;
            }

            let mut iter = line.split(':').map(|x| x.trim());

            let field_name = iter
                .next()
                .ok_or("Failed to read memory information, malformed /proc/meminfo output")?;
            let field_value = iter.next();

            if field_value.is_none() {
                continue;
            }

            let field_value = field_value
                .unwrap()
                .split_whitespace()
                .collect::<Vec<&str>>();

            match field_name.to_lowercase().as_str() {
                "memtotal" => {
                    result.total = parse_sized_field(field_name, &field_value)?;
                }
                "memfree" => {
                    result.free = parse_sized_field(field_name, &field_value)?;
                }
                "swaptotal" => {
                    result.swap_total = parse_sized_field(field_name, &field_value)?;
                }
                "swapfree" => {
                    result.swap_free = parse_sized_field(field_name, &field_value)?;
                }
                _ => {}
            }
        }

        result.used = result.total - result.free;
        result.swap_used = result.swap_total - result.swap_free;

        Ok(result)
    }
}
