use lazy_static::lazy_static;
use std::collections::HashMap;

mod statvfs_sys {
    #![allow(non_upper_case_globals)]
    #![allow(non_camel_case_types)]
    #![allow(non_snake_case)]
    #![allow(dead_code)]

    include!(concat!(env!("OUT_DIR"), "/statvfs-sys.rs"));
}

struct UserCache {
    pub users: HashMap<usize, String>,
    pub groups: HashMap<usize, String>,
}

impl UserCache {
    pub fn read(path: &str) -> Result<HashMap<usize, String>, String> {
        use std::fs::OpenOptions;
        use std::io::{BufRead, BufReader};

        let mut result = HashMap::new();

        let file = OpenOptions::new()
            .read(true)
            .open(path)
            .map_err(|e| e.to_string())?;
        let mut reader = BufReader::new(file);
        loop {
            let mut line = String::new();
            let size = reader.read_line(&mut line).map_err(|e| e.to_string())?;

            if size == 0 {
                break;
            }

            if line.is_empty() {
                continue;
            }

            let mut name = String::new();
            for (index, field) in line.split(':').enumerate() {
                if index == 0 {
                    name = field.to_string();
                    continue;
                }

                if index == 2 {
                    let id = field.parse::<usize>().map_err(|e| e.to_string())?;

                    result.insert(id, name);

                    break;
                }
            }
        }

        Ok(result)
    }
}

lazy_static! {
    static ref USER_CACHE: UserCache = {
        UserCache {
            users: UserCache::read("/etc/passwd").unwrap(),
            groups: UserCache::read("/etc/group").unwrap(),
        }
    };
}

pub fn parse_sized_field(field_name: &str, field_value: &Vec<&str>) -> Result<u64, String> {
    if field_value.len() != 2 {
        return Err(format!("Failed to parse field {}, malformed input", field_name).into());
    }

    let value = field_value[0].parse::<u64>().map_err(|e| e.to_string())?;
    match field_value[1].to_lowercase().as_str() {
        "b" => Ok(value),
        "kb" => Ok(value * 1000),
        "mb" => Ok(value * 1000 * 1000),
        "gb" => Ok(value * 1000 * 1000 * 1000),
        "tb" => Ok(value * 1000 * 1000 * 1000 * 1000),
        "kib" => Ok(value * 1024),
        "mib" => Ok(value * 1024 * 1024),
        "gib" => Ok(value * 1024 * 1024 * 1024),
        "tib" => Ok(value * 1024 * 1024 * 1024 * 1024),
        _ => Err("Unknown data type encountered when reading MemTotal".into()),
    }
}

pub fn user_name(user_id: usize) -> Option<&'static String> {
    USER_CACHE.users.get(&user_id)
}

pub fn group_name(group_id: usize) -> Option<&'static String> {
    USER_CACHE.groups.get(&group_id)
}

pub fn filesystem_usage(mount_point: &str, size_bytes: u64) -> Result<u64, String> {
    if mount_point.is_empty() {
        return Ok(0);
    }

    let mut stat = statvfs_sys::statvfs {
        f_bsize: 0,
        f_frsize: 0,
        f_blocks: 0,
        f_bfree: 0,
        f_bavail: 0,
        f_files: 0,
        f_ffree: 0,
        f_favail: 0,
        f_fsid: 0,
        f_flag: 0,
        f_namemax: 0,
        __f_spare: [0; 6usize],
    };

    let mount_point = format!("{}\0", mount_point);
    let result = unsafe { statvfs_sys::statvfs(mount_point.as_ptr() as *const i8, &mut stat) };
    if result != 0 {
        return Err("Failed to read space information".to_string());
    }

    let free_space = stat.f_bsize * stat.f_bavail;

    Ok(size_bytes - free_space)
}
