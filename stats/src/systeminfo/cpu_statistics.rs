use lazy_static::lazy_static;
use serde::Serialize;
use std::error::Error;

lazy_static! {
    static ref SENSORS: crate::systeminfo::sensors::Sensors = {
        let sensors = crate::systeminfo::sensors::new();
        if sensors.is_err() {
            panic!(
                "Failed to initialize sensor data: {}",
                sensors.err().unwrap()
            );
        }

        sensors.unwrap()
    };
}

#[derive(Debug, Serialize)]
pub struct Statistics {
    pub processor: String,
    pub physical_id: i32,
    pub logical_id: i32,
    pub active_percent: f32,
    pub idle_percent: f32,
    pub frequency: f32,
    pub temperature_current: f64,
    pub temperature_threshold: f64,
}

#[allow(dead_code)]
#[derive(Copy, Clone)]
pub enum GatherOptions {
    UsageAverage,
    UsageAllCPUs,
}

impl Statistics {
    pub fn gather(gather_options: GatherOptions) -> Result<Vec<Statistics>, String> {
        use std::thread::sleep;
        use std::time::Duration;

        let snapshot1 = CPUData::gather(gather_options.clone()).map_err(|e| e.to_string())?;
        sleep(Duration::from_millis(100));
        let snapshot2 = CPUData::gather(gather_options).map_err(|e| e.to_string())?;

        CPUData::calculate_usage(&snapshot1, &snapshot2).map_err(|e| e.to_string())
    }
}

#[allow(non_camel_case_types)]
#[derive(PartialEq, Copy, Clone)]
pub enum CPUStates {
    S_USER = 0,
    S_NICE,
    S_SYSTEM,
    S_IDLE,
    S_IOWAIT,
    S_IRQ,
    S_SOFTIRQ,
    S_STEAL,
    S_GUEST,
    S_GUEST_NICE,
    COUNT,
}

impl CPUStates {
    pub fn index(&self) -> usize {
        *self as usize
    }
}

pub const CPU_STATE_COUNT: usize = CPUStates::COUNT as usize;

struct CPUTemperature {
    pub current: f64,
    pub max: f64,
}

#[derive(Clone)]
pub struct CPUData {
    processor: String,
    times: [usize; CPU_STATE_COUNT],
}

impl CPUData {
    fn new() -> Self {
        Self {
            processor: "".to_string(),
            times: [0; CPU_STATE_COUNT],
        }
    }

    pub fn gather(gather_options: GatherOptions) -> Result<Vec<Self>, Box<dyn Error>> {
        use std::fs::OpenOptions;
        use std::io::{BufRead, BufReader};

        let mut result = Vec::new();

        let f = OpenOptions::new().read(true).open("/proc/stat")?;
        let mut stat_reader = BufReader::new(f);

        loop {
            let mut line = String::new();
            let size = stat_reader.read_line(&mut line)?;

            if size == 0 || !line.starts_with("cpu") {
                break;
            }

            result.push(CPUData::new());
            let cpu_data_entry = result.last_mut().unwrap();

            for (index, column) in line.split_whitespace().enumerate() {
                if index == 0 {
                    cpu_data_entry.processor = if column == "cpu" {
                        "TOTAL".to_string()
                    } else {
                        "CORE ".to_string() + (&column[3..])
                    };
                } else {
                    cpu_data_entry.times[index - 1] = column.parse::<usize>()?;
                }
            }

            match &gather_options {
                GatherOptions::UsageAverage => {
                    if cpu_data_entry.processor == "TOTAL" {
                        break;
                    }
                }
                _ => {}
            }
        }

        result.sort_unstable_by(|a, b| {
            use std::cmp::Ordering;

            if a.processor.as_str() == "TOTAL" {
                return Ordering::Less;
            }

            if b.processor.as_str() == "TOTAL" {
                return Ordering::Greater;
            }

            let core_number_a = a
                .processor
                .split_whitespace()
                .skip(1)
                .next()
                .unwrap()
                .parse::<i32>()
                .unwrap();
            let core_number_b = b
                .processor
                .split_whitespace()
                .skip(1)
                .next()
                .unwrap()
                .parse::<i32>()
                .unwrap();
            core_number_a.cmp(&core_number_b)
        });

        match gather_options {
            GatherOptions::UsageAverage => {
                return Ok(Vec::from(&result[0..1]));
            }
            _ => {}
        }

        Ok(result)
    }

    pub fn total_cpu_times() -> Result<Self, Box<dyn Error>> {
        use std::fs::OpenOptions;
        use std::io::{BufRead, BufReader};

        let mut result = Self::new();

        let f = OpenOptions::new().read(true).open("/proc/stat")?;
        let mut stat_reader = BufReader::new(f);

        let mut data_read = false;
        loop {
            let mut line = String::new();
            let size = stat_reader.read_line(&mut line)?;

            if size == 0 || !line.starts_with("cpu") {
                break;
            }

            for (index, column) in line.split_whitespace().enumerate() {
                if index == 0 {
                    if column == "cpu" {
                        data_read = true;
                    }
                } else {
                    result.times[index - 1] = column.parse::<usize>()?;
                }
            }

            if data_read {
                break;
            }
        }

        if data_read {
            Ok(result)
        } else {
            Err("Failed to read CPU information")?
        }
    }

    pub fn get_idle_time(&self) -> usize {
        self.times[CPUStates::S_IDLE.index()] + self.times[CPUStates::S_IOWAIT.index()]
    }

    pub fn get_active_time(&self) -> usize {
        self.times[CPUStates::S_USER.index()]
            + self.times[CPUStates::S_NICE.index()]
            + self.times[CPUStates::S_SYSTEM.index()]
            + self.times[CPUStates::S_IRQ.index()]
            + self.times[CPUStates::S_SOFTIRQ.index()]
            + self.times[CPUStates::S_STEAL.index()]
            + self.times[CPUStates::S_GUEST.index()]
            + self.times[CPUStates::S_GUEST_NICE.index()]
    }

    fn calculate_temperatures() -> Result<(CPUTemperature, Vec<CPUTemperature>), Box<dyn Error>> {
        use crate::systeminfo::sensors;
        use crate::systeminfo::sensors::SubFeatureType;

        let sensor_list = SENSORS.get_detected_chips().unwrap();

        let chip = sensor_list
            .iter()
            .find(|chip| chip.prefix() == "coretemp" || chip.prefix() == "k10temp")
            .ok_or("Failed to find CPU temperature info, missing 'coretemp' chip name")?;

        let mut chip_temp = CPUTemperature {
            current: 0.0,
            max: 0.0,
        };
        let mut result = Vec::new();

        for (index, feature) in chip
            .features()
            .map_err(|e| e.to_string())?
            .iter()
            .filter_map(|f| {
                if f.r#type() == sensors::FeatureType::Temperature {
                    Some(f.clone())
                } else {
                    None
                }
            })
            .enumerate()
        {
            let current_temperature = match feature.subfeature(SubFeatureType::TemperatureInput) {
                Ok(sub_feature) => match sub_feature.get_value() {
                    Ok(value) => value,
                    Err(e) => {
                        eprintln!("Failed to get chip temperature: {}", e.to_string());
                        0_f64
                    }
                },
                Err(e) => {
                    eprintln!("Failed to get chip temperature: {}", e.to_string());
                    0_f64
                }
            };

            let max_temperature = match feature.subfeature(SubFeatureType::TemperatureMaximum) {
                Ok(sub_feature) => match sub_feature.get_value() {
                    Ok(value) => value,
                    Err(e) => {
                        eprintln!("Failed to get chip temperature: {}", e.to_string());
                        0_f64
                    }
                },
                Err(e) => {
                    eprintln!("Failed to get chip temperature: {}", e.to_string());
                    0_f64
                }
            };

            if index == 0 {
                chip_temp.current = current_temperature;
                chip_temp.max = max_temperature;
                continue;
            }

            result.push(CPUTemperature {
                current: current_temperature,
                max: max_temperature,
            })
        }

        Ok((chip_temp, result))
    }

    pub fn calculate_usage(
        snapshot1: &Vec<Self>,
        snapshot2: &Vec<Self>,
    ) -> Result<Vec<Statistics>, Box<dyn Error>> {
        if snapshot1.len() != snapshot2.len() {
            Err("Snapshots taken from different CPUs")?;
        }

        let entry_count = snapshot1.len();

        let mut result = Vec::new();

        let cpu_info = crate::systeminfo::cpu_info::CPUInfo::gather()?;
        let mut average_freq = 0.0;

        let (chip_temp, core_temps) = Self::calculate_temperatures()?;

        for i in 0..entry_count {
            let cpu_data1 = &snapshot1[i];
            let cpu_data2 = &snapshot2[i];

            if cpu_data1.processor != cpu_data2.processor {
                Err("Snapshots taken from different CPUs")?;
            }

            let active_time =
                cpu_data2.get_active_time() as f32 - cpu_data1.get_active_time() as f32;
            let idle_time = cpu_data2.get_idle_time() as f32 - cpu_data1.get_idle_time() as f32;
            let total_time = active_time + idle_time;

            result.push(Statistics {
                processor: cpu_data1.processor.clone(),
                physical_id: if i > 0 {
                    cpu_info[i - 1].core_id as i32
                } else {
                    -1
                },
                logical_id: if i > 0 {
                    cpu_info[i - 1].logical_id as i32
                } else {
                    -1
                },
                active_percent: 100.0 * active_time / total_time,
                idle_percent: 100.0 * idle_time / total_time,
                frequency: if i > 0 {
                    average_freq += cpu_info[i - 1].current_frequency;
                    cpu_info[i - 1].current_frequency
                } else {
                    0.0
                },
                temperature_current: if i > 0 {
                    let index = cpu_info[i - 1].core_id as usize;
                    if index >= core_temps.len() {
                        0 as f64
                    } else {
                        core_temps[index].current
                    }
                } else {
                    chip_temp.current
                },
                temperature_threshold: if i > 0 {
                    let index = cpu_info[i - 1].core_id as usize;
                    if index >= core_temps.len() {
                        0 as f64
                    } else {
                        core_temps[index].max
                    }
                } else {
                    chip_temp.max
                },
            });
        }

        result[0].processor = cpu_info[0].model_name.clone();
        result[0].frequency = average_freq / cpu_info.len() as f32;

        Ok(result)
    }
}
