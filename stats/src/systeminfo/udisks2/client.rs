use crate::systeminfo::udisks2::block::Block;
use crate::systeminfo::udisks2::drive::Drive;
use crate::systeminfo::udisks2::gobject_list::GObjectList;
use crate::systeminfo::udisks2::object::Object;
use crate::systeminfo::udisks2::sys::*;

pub struct Client {
    client: *mut UDisksClient,
    manager: *mut UDisksManager,
    manager_arg_options: *mut GVariant,
}

impl Client {
    pub fn new() -> Result<Self, String> {
        use std::ffi::CStr;

        let mut error: *mut GError = std::ptr::null_mut();

        let client = unsafe { udisks_client_new_sync(std::ptr::null_mut(), &mut error) };
        if !error.is_null() {
            let error_string = unsafe { CStr::from_ptr((*error).message) }
                .to_string_lossy()
                .to_string();
            unsafe { g_error_free(error) };
            return Err(error_string);
        }

        let mut arg_options_builder: GVariantBuilder =
            unsafe { std::mem::MaybeUninit::uninit().assume_init() };

        unsafe {
            g_variant_builder_init(
                &mut arg_options_builder,
                g_variant_type_checked_("a{sv}\0".as_ptr() as *const gchar),
            );
            g_variant_builder_open(
                &mut arg_options_builder,
                g_variant_type_checked_("{sv}\0".as_ptr() as *const gchar),
            );
            g_variant_builder_add(
                &mut arg_options_builder,
                "s\0".as_ptr() as *const gchar,
                "auth.no_user_interaction\0".as_ptr() as *const gchar,
            );
            g_variant_builder_add(
                &mut arg_options_builder,
                "v\0".as_ptr() as *const gchar,
                g_variant_new_boolean(0),
            );
            g_variant_builder_close(&mut arg_options_builder);
        }

        let arg_options = unsafe { g_variant_builder_end(&mut arg_options_builder) };
        unsafe { g_variant_take_ref(arg_options) };

        Ok(Self {
            client,
            manager: unsafe { udisks_client_get_manager(client) },
            manager_arg_options: arg_options,
        })
    }

    pub fn make_object(&self, path: *const gchar) -> Result<Object, String> {
        use std::ffi::CStr;

        let object = unsafe { udisks_client_get_object(self.client, path) };
        if object.is_null() {
            return Err(format!(
                "Failed to find block device at path {}",
                unsafe { CStr::from_ptr(path) }.to_string_lossy()
            ));
        }

        Ok(Object::take(path, object))
    }

    pub fn make_drive(&self, block: &Block) -> Option<Drive> {
        let drive_path = unsafe { udisks_block_get_drive(block.handle()) };
        let drive = unsafe { udisks_client_get_drive_for_block(self.client, block.handle()) };

        if drive.is_null() {
            return None;
        }

        let drive_object = unsafe { udisks_client_peek_object(self.client, drive_path) };
        let drive_ata = unsafe { udisks_object_get_drive_ata(drive_object) };

        Some(Drive::take(drive_path, drive, drive_ata))
    }

    pub fn block_device_object_paths(&self) -> Result<GObjectList<gchar>, String> {
        use std::ffi::CStr;

        let mut error: *mut GError = std::ptr::null_mut();

        let mut block_device_objects: *mut *mut gchar = std::ptr::null_mut();
        let _ = unsafe {
            udisks_manager_call_get_block_devices_sync(
                self.manager,
                self.manager_arg_options,
                &mut block_device_objects,
                std::ptr::null_mut(),
                &mut error,
            )
        };

        if !error.is_null() {
            let error_string = unsafe { CStr::from_ptr((*error).message) }
                .to_string_lossy()
                .to_string();
            unsafe { g_error_free(error) };
            return Err(error_string);
        }

        Ok(GObjectList::take(block_device_objects, g_free))
    }
}

impl Drop for Client {
    fn drop(&mut self) {
        if !self.client.is_null() {
            unsafe { g_variant_unref(self.manager_arg_options) }
            unsafe { g_object_unref(self.client as *mut _) }
            // Manager does not need to be freed according to the docs
        }
    }
}

unsafe impl std::marker::Sync for Client {}
