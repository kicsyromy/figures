use crate::systeminfo::udisks2::sys::*;

pub struct Block {
    handle: *mut UDisksBlock,
}

impl Block {
    pub fn take(handle: *mut UDisksBlock) -> Self {
        Block { handle }
    }

    pub fn r#type(&self) -> String {
        use std::ffi::CStr;

        unsafe { CStr::from_ptr(udisks_block_get_id_type(self.handle)) }
            .to_string_lossy()
            .to_string()
    }

    pub fn device_file(&self) -> String {
        use std::ffi::CStr;

        unsafe { CStr::from_ptr(udisks_block_get_device(self.handle)) }
            .to_string_lossy()
            .to_string()
    }

    pub fn size(&self) -> u64 {
        unsafe { udisks_block_get_size(self.handle) }
    }

    pub fn label(&self) -> String {
        use std::ffi::CStr;

        unsafe { CStr::from_ptr(udisks_block_get_id_label(self.handle)) }
            .to_string_lossy()
            .to_string()
    }

    pub fn uuid(&self) -> String {
        use std::ffi::CStr;

        unsafe { CStr::from_ptr(udisks_block_get_id_uuid(self.handle)) }
            .to_string_lossy()
            .to_string()
    }

    pub fn handle(&self) -> *mut UDisksBlock {
        self.handle
    }
}

impl Drop for Block {
    fn drop(&mut self) {
        if !self.handle.is_null() {
            unsafe { g_object_unref(self.handle as *mut _) }
        }
    }
}
