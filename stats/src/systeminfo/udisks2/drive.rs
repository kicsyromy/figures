use crate::systeminfo::udisks2::sys::*;
use std::hash::Hasher;

pub struct Drive {
    path: String,
    handle: *mut UDisksDrive,
    ata_handle: *mut UDisksDriveAta,
}

impl Drive {
    pub fn take(
        path: *const gchar,
        handle: *mut UDisksDrive,
        ata_handle: *mut UDisksDriveAta,
    ) -> Self {
        use std::ffi::CStr;

        Self {
            path: unsafe { CStr::from_ptr(path) }
                .to_string_lossy()
                .to_string(),
            handle,
            ata_handle,
        }
    }

    pub fn model(&self) -> String {
        use std::ffi::CStr;

        unsafe { CStr::from_ptr(udisks_drive_get_model(self.handle)) }
            .to_string_lossy()
            .to_string()
    }

    pub fn serial(&self) -> String {
        use std::ffi::CStr;

        unsafe { CStr::from_ptr(udisks_drive_get_serial(self.handle)) }
            .to_string_lossy()
            .to_string()
    }

    pub fn capacity(&self) -> u64 {
        unsafe { udisks_drive_get_size(self.handle) }
    }

    pub fn has_smart_info(&self) -> bool {
        !self.ata_handle.is_null()
            && unsafe { udisks_drive_ata_get_smart_supported(self.ata_handle) != 0 }
            && unsafe { udisks_drive_ata_get_smart_enabled(self.ata_handle) != 0 }
    }

    pub fn temperature_celsius(&self) -> f64 {
        if !self.ata_handle.is_null() {
            unsafe { udisks_drive_ata_get_smart_temperature(self.ata_handle) - 273.15_f64 }
        } else {
            -1_f64
        }
    }

    pub fn selftest_status(&self) -> String {
        use std::ffi::CStr;

        if !self.ata_handle.is_null() {
            unsafe { CStr::from_ptr(udisks_drive_ata_get_smart_selftest_status(self.ata_handle)) }
                .to_string_lossy()
                .to_string()
        } else {
            "".to_string()
        }
    }
}

impl Drop for Drive {
    fn drop(&mut self) {
        if !self.handle.is_null() {
            unsafe { g_object_unref(self.handle as *mut _) }
        }
    }
}

impl std::hash::Hash for Drive {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.path.hash(state)
    }
}

impl std::cmp::PartialEq for Drive {
    fn eq(&self, other: &Self) -> bool {
        self.path == other.path
    }
}

impl std::cmp::Eq for Drive {}
