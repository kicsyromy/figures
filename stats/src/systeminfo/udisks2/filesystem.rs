use crate::systeminfo::udisks2::gobject_list::GObjectList;
use crate::systeminfo::udisks2::sys::*;

pub struct Filesystem {
    handle: *mut UDisksFilesystem,
}

impl Filesystem {
    pub fn take(handle: *mut UDisksFilesystem) -> Self {
        Self { handle }
    }

    pub fn mount_points(&self) -> Vec<String> {
        use std::ffi::CStr;

        let mut result = Vec::new();

        let mount_points = unsafe { udisks_filesystem_get_mount_points(self.handle) };
        let mount_points = GObjectList::wrap(mount_points);

        for mount_point in mount_points {
            let mp = unsafe { CStr::from_ptr(mount_point) }
                .to_string_lossy()
                .to_string();

            if !result.contains(&mp) {
                result.push(mp);
            }
        }

        result
    }

    pub fn size_bytes(&self) -> u64 {
        let mut size_bytes = 0_u64;
        unsafe {
            g_object_get(
                self.handle as *mut _,
                "size\0".as_ptr() as *const gchar,
                &mut size_bytes,
                0,
            )
        };

        size_bytes
    }
}

impl Drop for Filesystem {
    fn drop(&mut self) {
        if !self.handle.is_null() {
            unsafe { g_object_unref(self.handle as *mut _) }
        }
    }
}
