use crate::systeminfo::udisks2::block::Block;
use crate::systeminfo::udisks2::filesystem::Filesystem;
use crate::systeminfo::udisks2::sys::*;

pub struct Object {
    _path: String,
    handle: *mut UDisksObject,
}

impl Object {
    pub fn take(path: *const gchar, handle: *mut UDisksObject) -> Self {
        use std::ffi::CStr;

        Self {
            _path: unsafe { CStr::from_ptr(path) }
                .to_string_lossy()
                .to_string(),
            handle,
        }
    }

    pub fn as_block(&self) -> Option<Block> {
        let block_device = unsafe { udisks_object_get_block(self.handle) };
        if block_device.is_null() {
            return None;
        }

        Some(Block::take(block_device))
    }

    pub fn as_filesystem(&self) -> Option<Filesystem> {
        let filesystem = unsafe { udisks_object_get_filesystem(self.handle) };
        if filesystem.is_null() {
            return None;
        }

        Some(Filesystem::take(filesystem))
    }

    pub fn _path(&self) -> &str {
        self._path.as_str()
    }
}

impl Drop for Object {
    fn drop(&mut self) {
        if !self.handle.is_null() {
            unsafe { g_object_unref(self.handle as *mut _) }
        }
    }
}
