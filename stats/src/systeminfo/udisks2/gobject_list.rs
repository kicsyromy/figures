use crate::systeminfo::udisks2::sys::*;

pub struct GObjectList<T> {
    content: *const *const T,
    current_position: *const *const T,
    free_function: Option<unsafe extern "C" fn(gpointer) -> ()>,
}

impl<T> GObjectList<T> {
    pub fn wrap(list: *const *const T) -> Self {
        GObjectList {
            content: list as usize as *const *const T,
            current_position: list as usize as *const *const T,
            free_function: None,
        }
    }

    pub fn take(list: *mut *mut T, free_function: unsafe extern "C" fn(gpointer) -> ()) -> Self {
        GObjectList {
            content: list as usize as *const *const T,
            current_position: list as usize as *const *const T,
            free_function: Some(free_function),
        }
    }

    pub fn reset(&mut self) {
        self.current_position = self.content;
    }
}

impl<T> Iterator for GObjectList<T> {
    type Item = *const T;

    fn next(&mut self) -> Option<Self::Item> {
        let current_item = unsafe { *self.current_position };

        let result = if current_item.is_null() {
            None
        } else {
            self.current_position = (self.current_position as usize
                + std::mem::size_of::<*const T>())
                as *const *const T;
            Some(current_item)
        };

        result
    }
}

impl<T> Drop for GObjectList<T> {
    fn drop(&mut self) {
        if self.free_function.is_some() {
            self.reset();

            loop {
                let current_item = self.next();

                if current_item.is_none() {
                    break;
                }

                unsafe { self.free_function.unwrap()(current_item.unwrap() as usize as *mut _) };
            }
            unsafe { self.free_function.unwrap()(self.content as usize as *mut _) };
        }
    }
}
