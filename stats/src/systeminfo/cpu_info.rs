use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct CPUInfo {
    pub physical_id: u16,
    pub logical_id: u16,
    pub core_id: u16,
    pub vendor_id: String,
    pub model_name: String,
    pub current_frequency: f32,
}

impl CPUInfo {
    fn new() -> Self {
        CPUInfo {
            physical_id: 0,
            logical_id: 0,
            core_id: 0,
            vendor_id: "".to_string(),
            model_name: "".to_string(),
            current_frequency: 0.0,
        }
    }

    pub fn core_count() -> Result<u64, Box<dyn std::error::Error>> {
        use std::fs::File;
        use std::io::{BufRead, BufReader};

        let f = File::open("/proc/cpuinfo")?;
        let mut freq_reader = BufReader::new(f);

        let mut result = 0u64;

        loop {
            let mut line = String::new();
            let size = freq_reader.read_line(&mut line)?;

            if size == 0 {
                break;
            }

            if line.is_empty() {
                continue;
            }

            if line.to_lowercase().trim().starts_with("processor") {
                result += 1;
            }
        }

        Ok(result)
    }

    pub fn gather() -> Result<Vec<Self>, Box<dyn std::error::Error>> {
        use std::fs::File;
        use std::io::{BufRead, BufReader};

        let mut result = Vec::new();

        let f = File::open("/proc/cpuinfo")?;
        let mut freq_reader = BufReader::new(f);

        let mut last_element = &mut CPUInfo {
            physical_id: 0,
            logical_id: 0,
            core_id: 0,
            vendor_id: "".to_string(),
            model_name: "".to_string(),
            current_frequency: 0.0,
        };

        loop {
            let mut line = String::new();
            let size = freq_reader.read_line(&mut line)?;

            if size == 0 {
                break;
            }

            if line.is_empty() {
                continue;
            }

            // for (index, text) in line.split(':').map(|x| x.trim()).enumerate() {
            let mut iter = line.split(':').map(|x| x.trim());

            let field_name = iter
                .next()
                .ok_or("Failed to read processor information, malformed /proc/cpuinfo output")?;
            let field_value = iter.next();

            if field_value.is_none() {
                continue;
            }

            let field_value = field_value.unwrap();

            match field_name.to_lowercase().as_str() {
                "processor" => {
                    result.push(CPUInfo::new());
                    last_element = result.last_mut().unwrap();
                    last_element.logical_id = field_value.parse()?;
                }
                "vendor_id" => {
                    last_element.vendor_id = field_value.to_string();
                }
                "model name" => {
                    last_element.model_name = field_value.to_string();
                }
                "cpu mhz" => {
                    last_element.current_frequency = field_value.parse()?;
                }
                "physical id" => {
                    last_element.physical_id = field_value.parse()?;
                }
                "core id" => {
                    last_element.core_id = field_value.parse()?;
                }
                _ => {}
            }
        }

        result.sort_unstable_by(|left, right| left.logical_id.cmp(&right.logical_id));

        Ok(result)
    }
}
