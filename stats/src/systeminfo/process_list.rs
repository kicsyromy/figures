use serde::Serialize;
use std::io::Read;

#[derive(Clone, Debug, Serialize, PartialEq)]
pub enum State {
    Running,
    Sleeping,
    Waiting,
    Zombie,
    Stopped,
    Tracing,
    Dead,
    Unknown,
}

#[derive(Debug, PartialEq, Clone)]
#[repr(u32)]
pub enum Error {
    FilteredOut,
    FileNotFound,
    SmapsPermissionDenied,
    ParseError(String),
    InputError(String),
    Other(String),
}

impl std::convert::From<String> for Error {
    fn from(message: String) -> Self {
        Error::Other(message)
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let fmt_result = match self {
            Error::FilteredOut => f.write_str("Process does not match any of the filters"),
            Error::FileNotFound => f.write_str("Process terminated before data could be gathered"),
            Error::SmapsPermissionDenied => {
                f.write_str("Permission denied when reading memory information")
            }
            Error::ParseError(err_string) => f.write_str(err_string.as_str()),
            Error::InputError(err_string) => f.write_str(err_string.as_str()),
            Error::Other(err_string) => f.write_str(err_string.as_str()),
        };

        fmt_result
    }
}

// Note to self: parse /proc/[pid]/stat or /proc/[pid]/status for general info
//               parse /proc/[pid]/smaps_rollup for memory info
#[derive(Clone, Debug, Serialize)]
pub struct Process {
    pub name: String,
    pub command_line: String,
    pub pid: usize,
    pub state: State,
    pub user_id: usize,
    pub group_id: usize,
    pub user_name: String,
    pub group_name: String,
    pub memory_rss: u64,
    pub memory_pss: u64,
    pub memory_shared: u64,
    pub memory_private: u64,
    pub memory_swapped_out: u64,
    pub cpu_usage_percent: f32,

    #[serde(skip_serializing)]
    pub utime: usize,
    #[serde(skip_serializing)]
    pub stime: usize,
}

impl Process {
    fn check_filters(process_name: &str, filters: &[(bool, regex::Regex)]) -> bool {
        // NOTE: This works because filters come in pre-sorted
        //       positive filters first followed by negative filters
        let mut keep = filters[0].0;
        for (negate, regex) in filters {
            if *negate {
                keep = keep && !(regex.is_match(process_name) && *negate);
                if !keep {
                    break;
                }
            } else {
                keep = keep || regex.is_match(process_name);
            }
        }

        keep
    }

    fn read_cmd_line(
        &mut self,
        mut file: &std::fs::File,
        filters: &Option<&[(bool, regex::Regex)]>,
    ) -> Result<(), Error> {
        use std::path::PathBuf;

        let mut cmdline = String::new();

        file.read_to_string(&mut cmdline)
            .map_err(|e| e.to_string())?;

        if !cmdline.is_empty() {
            for s in cmdline.split('\0').map(|s| s.trim()) {
                if !s.is_empty() {
                    self.command_line += s;
                    self.command_line += " ";
                }
            }
            self.command_line.remove(self.command_line.len() - 1);

            let potential_name = self.command_line.split_whitespace().next().unwrap();

            let path = PathBuf::from(potential_name);
            if path.exists() && path.is_file() {
                self.name = path
                    .file_name()
                    .unwrap()
                    .to_str()
                    .unwrap_or_default()
                    .to_string();
            } else {
                self.name = potential_name
                    .split(':')
                    .next()
                    .unwrap()
                    .split('/')
                    .last()
                    .unwrap()
                    .to_string();
            }

            if filters.is_some() {
                if !Self::check_filters(self.name.as_str(), filters.unwrap()) {
                    return Err(Error::FilteredOut);
                }
            }
        }

        Ok(())
    }

    fn read_status(
        &mut self,
        file: &std::fs::File,
        filters: &Option<&[(bool, regex::Regex)]>,
    ) -> Result<(), Error> {
        use crate::systeminfo::utilities::{group_name, user_name};
        use std::io::{BufRead, BufReader};

        let mut stat_reader = BufReader::new(file);

        loop {
            let mut line = String::new();
            let size = stat_reader
                .read_line(&mut line)
                .map_err(|e| e.to_string())?;

            if size == 0 {
                break;
            }

            if line.is_empty() {
                continue;
            }

            let mut iter = line.split(':').map(|x| x.trim());

            let field_name = iter.next().ok_or(Error::InputError(format!(
                "Failed to read self.information for {pid}, malformed /proc/{pid}/status output",
                pid = self.pid
            )))?;
            let field_value = iter.next();

            if field_value.is_none() {
                continue;
            }

            let field_value = field_value.unwrap();

            match field_name.to_lowercase().as_str() {
                "name" => {
                    if self.name.is_empty() {
                        self.name = field_value.to_string();

                        if filters.is_some() {
                            if !Self::check_filters(self.name.as_str(), filters.unwrap()) {
                                return Err(Error::FilteredOut);
                            }
                        }
                    }
                }
                "state" => {
                    self.state = match field_value.split_whitespace().next().unwrap() {
                        "R" => State::Running,
                        "S" => State::Sleeping,
                        "D" => State::Waiting,
                        "Z" => State::Zombie,
                        "T" => State::Stopped,
                        "t" => State::Tracing,
                        "X" => State::Dead,
                        _ => State::Unknown,
                    };
                }
                "uid" => {
                    let field_value = field_value.split_whitespace().next();
                    match field_value {
                        None => {
                            return Err(Error::InputError(format!(
                                "Invalid uid in /proc/{}/status",
                                self.pid
                            )));
                        }
                        Some(field_value) => {
                            self.user_id =
                                field_value.parse::<usize>().map_err(|e| e.to_string())?;
                            self.user_name =
                                user_name(self.user_id).unwrap_or(&"".to_string()).clone();
                        }
                    }
                }
                "gid" => {
                    let field_value = field_value.split_whitespace().next();
                    match field_value {
                        None => {
                            return Err(Error::InputError(format!(
                                "Invalid gid in /proc/{}/status",
                                self.pid
                            )));
                        }
                        Some(field_value) => {
                            self.group_id =
                                field_value.parse::<usize>().map_err(|e| e.to_string())?;
                            self.group_name =
                                group_name(self.group_id).unwrap_or(&"".to_string()).clone();
                        }
                    }
                }
                _ => {}
            }
        }

        Ok(())
    }

    fn read_smaps(&mut self, file: &std::fs::File) -> Result<(), Error> {
        use crate::systeminfo::utilities::parse_sized_field;
        use std::io::{BufRead, BufReader};

        let mut meminfo_reader = BufReader::new(file);

        let mut shared_clean = 0u64;
        let mut shared_dirty = 0u64;
        let mut private_clean = 0u64;
        let mut private_dirty = 0u64;
        loop {
            let mut line = String::new();
            let size = meminfo_reader
                .read_line(&mut line)
                .map_err(|e| e.to_string());

            if size.is_err() || size.unwrap() == 0 {
                break;
            }

            if line.is_empty() || line.contains("[rollup]") {
                continue;
            }

            let mut iter = line.split(':').map(|x| x.trim());

            let field_name = iter.next().ok_or(format!(
                    "Failed to read self.information for {pid}, malformed /proc/{pid}/smaps_rollup output",
                    pid = self.pid
                ))?;
            let field_value = iter.next();

            if field_value.is_none() {
                continue;
            }

            let field_value = field_value
                .unwrap()
                .split_whitespace()
                .collect::<Vec<&str>>();

            match field_name.to_lowercase().as_str() {
                "rss" => {
                    self.memory_rss = parse_sized_field(field_name, &field_value)?;
                }
                "pss" => {
                    self.memory_pss = parse_sized_field(field_name, &field_value)?;
                }
                "shared_clean" => {
                    shared_clean = parse_sized_field(field_name, &field_value)?;
                }
                "shared_dirty" => {
                    shared_dirty = parse_sized_field(field_name, &field_value)?;
                }
                "private_clean" => {
                    private_clean = parse_sized_field(field_name, &field_value)?;
                }
                "private_dirty" => {
                    private_dirty = parse_sized_field(field_name, &field_value)?;
                }
                "swap" => {
                    self.memory_swapped_out = parse_sized_field(field_name, &field_value)?;
                }
                _ => {}
            }

            self.memory_shared = shared_clean + shared_dirty;
            self.memory_private = private_clean + private_dirty;
        }

        Ok(())
    }

    fn parse_value(value: Option<&str>, none_error_message: String) -> Result<usize, Error> {
        value
            .ok_or(Error::ParseError(none_error_message))?
            .parse::<usize>()
            .map_err(|e| Error::ParseError(e.to_string()))
    }

    /*
    (14) utime  %lu
         Amount of time that this process has been scheduled in
         user mode, measured in clock ticks (divide by
         sysconf(_SC_CLK_TCK)).  This includes guest time,
         guest_time (time spent running a virtual CPU, see be‐
         low), so that applications that are not aware of the
         guest time field do not lose that time from their cal‐
         culations.

    (15) stime  %lu
         Amount of time that this process has been scheduled in
         kernel mode, measured in clock ticks (divide by
         sysconf(_SC_CLK_TCK)).
     */
    fn read_cpu_times(pid: usize, mut file: &std::fs::File) -> Result<(usize, usize), Error> {
        const INDEX_UTIME: usize = 13;
        const _INDEX_STIME: usize = 14;

        let mut line = String::new();
        file.read_to_string(&mut line)
            .map_err(|e| Error::ParseError(e.to_string()))?;

        let mut iter = line.split_whitespace().skip(INDEX_UTIME);
        let utime = Self::parse_value(
            iter.next(),
            format!("Missing or invalid utime information in /proc/{}/stat", pid),
        )?;
        let stime = Self::parse_value(
            iter.next(),
            format!("Missing or invalid stime information in /proc/{}/stat", pid),
        )?;

        Ok((utime, stime))
    }

    fn read_stats(
        pid: usize,
        path: &std::path::Path,
        filters: Option<&[(bool, regex::Regex)]>,
    ) -> Result<Self, Error> {
        use std::fs::OpenOptions;

        let mut process = Self {
            name: String::new(),
            command_line: String::new(),
            pid,
            state: State::Unknown,
            user_id: 0,
            group_id: 0,
            user_name: String::new(),
            group_name: String::new(),
            memory_rss: 0,
            memory_pss: 0,
            memory_shared: 0,
            memory_private: 0,
            memory_swapped_out: 0,
            cpu_usage_percent: 0.0,
            utime: 0,
            stime: 0,
        };

        let open_options = OpenOptions::new().read(true).clone();

        let mut files = [
            open_options.open(path.join("cmdline")),
            open_options.open(path.join("status")),
            open_options.open(path.join("smaps_rollup")),
            open_options.open(path.join("stat")),
        ];
        let mut file_iterator = files.iter_mut().enumerate().map(|(index, f)| {
            if f.is_err() {
                let err = f.as_ref().unwrap_err();
                if err.kind() == std::io::ErrorKind::NotFound {
                    return Err(Error::FileNotFound);
                } else if index == 2 && err.kind() == std::io::ErrorKind::PermissionDenied {
                    return Err(Error::SmapsPermissionDenied);
                } else {
                    return Err(Error::Other(err.to_string()));
                }
            }

            Ok(f.as_ref().unwrap())
        });

        process.read_cmd_line(file_iterator.next().unwrap()?, &filters)?;
        process.read_status(file_iterator.next().unwrap()?, &filters)?;
        let smaps_file = file_iterator.next().unwrap();
        if smaps_file.is_err() {
            let err = smaps_file.unwrap_err();
            if err != Error::SmapsPermissionDenied {
                return Err(err);
            }
        } else {
            process.read_smaps(smaps_file.unwrap())?;
        }
        let cpu_times = Self::read_cpu_times(process.pid, file_iterator.next().unwrap()?)?;
        process.utime = cpu_times.0;
        process.stime = cpu_times.1;

        Ok(process)
    }

    pub fn gather(filters: Option<&[(bool, regex::Regex)]>) -> Result<Vec<Self>, String> {
        use crate::systeminfo::{cpu_info, cpu_statistics};
        use std::fs::read_dir;
        use std::fs::OpenOptions;
        use std::thread::sleep;
        use std::time::Duration;

        let proc_dir = read_dir("/proc").map_err(|e| e.to_string())?;

        let mut processes = Vec::new();

        for process_dir in proc_dir {
            let process = process_dir.map_err(|e| e.to_string())?;
            let r#type = process.file_type().map_err(|e| e.to_string())?;
            if r#type.is_dir() {
                let parsed_pid_str = process
                    .file_name()
                    .to_str()
                    .ok_or("Failed to get process id from /proc")?
                    .parse::<usize>();
                if parsed_pid_str.is_ok() {
                    let process = Self::read_stats(
                        parsed_pid_str.unwrap(),
                        process.path().as_path(),
                        filters,
                    );
                    if process.is_err() {
                        let err = process.unwrap_err();
                        if err == Error::FileNotFound || err == Error::FilteredOut {
                            continue;
                        } else {
                            return Err(err.to_string());
                        }
                    }

                    processes.push(process.unwrap());
                }
            }
        }

        let core_count = cpu_info::CPUInfo::core_count().map_err(|e| e.to_string())? as f32;

        let cpu_times_1 = cpu_statistics::CPUData::total_cpu_times().map_err(|e| e.to_string())?;
        let cpu_times_1 = (cpu_times_1.get_active_time() + cpu_times_1.get_idle_time()) as f32;

        sleep(Duration::from_millis(500));

        for process in &mut processes {
            // (number of processors) * (proc_times2 - proc_times1) * 100 / (float) (total_cpu_usage2 - total_cpu_usage1)
            let utime_1 = process.utime;
            let stime_1 = process.stime;
            let proc_times_1 = (utime_1 + stime_1) as f32;

            let file = OpenOptions::new()
                .read(true)
                .open(format!("/proc/{}/stat", process.pid));
            if file.is_err() {
                let err = file.unwrap_err();
                if err.kind() == std::io::ErrorKind::NotFound {
                    continue;
                } else {
                    return Err(err.to_string());
                }
            } else {
                let file = file.unwrap();

                let cpu_times_2 =
                    cpu_statistics::CPUData::total_cpu_times().map_err(|e| e.to_string())?;
                let cpu_times_2 =
                    (cpu_times_2.get_active_time() + cpu_times_2.get_idle_time()) as f32;

                let (utime_2, stime_2) =
                    Self::read_cpu_times(process.pid, &file).map_err(|e| e.to_string())?;

                let proc_times_2 = (utime_2 + stime_2) as f32;
                process.cpu_usage_percent = core_count * (proc_times_2 - proc_times_1) * 100f32
                    / (cpu_times_2 - cpu_times_1);
            }
        }

        Ok(processes)
    }
}
