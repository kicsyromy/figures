#![allow(dead_code)]

use core::fmt;
use core::str::Utf8Error;
use std::ffi::{CStr, CString};
use std::path::{Path, PathBuf};

mod sensors_sys {
    #![allow(non_upper_case_globals)]
    #![allow(non_camel_case_types)]
    #![allow(non_snake_case)]

    include!(concat!(env!("OUT_DIR"), "/sensors-sys.rs"));
}

bitflags::bitflags! {
    struct SensorFlags: u32 {
        const MODE_READ = sensors_sys::SENSORS_MODE_R;
        const MODE_WRITE = sensors_sys::SENSORS_MODE_W;
        const COMPUTE_MAPPING = sensors_sys::SENSORS_COMPUTE_MAPPING;
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
#[repr(u32)]
pub enum Error {
    Wildcards = sensors_sys::SENSORS_ERR_WILDCARDS,
    NoEntry = sensors_sys::SENSORS_ERR_NO_ENTRY,
    AccessRead = sensors_sys::SENSORS_ERR_ACCESS_R,
    Kernel = sensors_sys::SENSORS_ERR_KERNEL,
    DivisionByZero = sensors_sys::SENSORS_ERR_DIV_ZERO,
    ChipName = sensors_sys::SENSORS_ERR_CHIP_NAME,
    BusName = sensors_sys::SENSORS_ERR_BUS_NAME,
    Parse = sensors_sys::SENSORS_ERR_PARSE,
    AccessWrite = sensors_sys::SENSORS_ERR_ACCESS_W,
    IO = sensors_sys::SENSORS_ERR_IO,
    Recursion = sensors_sys::SENSORS_ERR_RECURSION,
    BusType = 100,
    InvalidName,
    NoSuchFeature,
    Unknown,
}

impl Error {
    pub fn from_sensors_error_number(error: i32) -> Self {
        let error = error.abs();
        match error as u32 {
            sensors_sys::SENSORS_ERR_WILDCARDS => Self::Wildcards,
            sensors_sys::SENSORS_ERR_NO_ENTRY => Self::NoEntry,
            sensors_sys::SENSORS_ERR_ACCESS_R => Self::AccessRead,
            sensors_sys::SENSORS_ERR_KERNEL => Self::Kernel,
            sensors_sys::SENSORS_ERR_DIV_ZERO => Self::DivisionByZero,
            sensors_sys::SENSORS_ERR_CHIP_NAME => Self::ChipName,
            sensors_sys::SENSORS_ERR_BUS_NAME => Self::BusName,
            sensors_sys::SENSORS_ERR_PARSE => Self::Parse,
            sensors_sys::SENSORS_ERR_ACCESS_W => Self::AccessWrite,
            sensors_sys::SENSORS_ERR_IO => Self::IO,
            sensors_sys::SENSORS_ERR_RECURSION => Self::Recursion,
            _ => Error::Unknown,
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let error_number = *self as i32;

        let fmt_result;
        if error_number < 100 {
            fmt_result = f.write_str(
                unsafe { CStr::from_ptr(sensors_sys::sensors_strerror(error_number)) }
                    .to_str()
                    .unwrap(),
            )
        } else {
            fmt_result = match *self {
                Error::BusType => f.write_str("Got an invalid BUS type for a chip"),
                Error::InvalidName => {
                    f.write_str("Name of chip, feature or subfeature is not valid UTF-8")
                }
                Error::NoSuchFeature => f.write_str("Invalid feature or subfeature requested"),
                _ => f.write_str("Unknown error"),
            }
        }

        fmt_result
    }
}

impl std::convert::From<std::str::Utf8Error> for Error {
    fn from(_: Utf8Error) -> Self {
        Self::InvalidName
    }
}

pub struct Sensors {}

pub fn new() -> Result<Sensors, Error> {
    let init_result = unsafe { sensors_sys::sensors_init(std::ptr::null_mut()) };
    if init_result != 0 {
        return Err(Error::from_sensors_error_number(init_result));
    }

    Ok(Sensors {})
}

impl Sensors {
    pub fn get_detected_chips(&self) -> Result<Vec<Chip>, Error> {
        let mut result = Vec::new();

        let mut number = 0;
        loop {
            let chip = unsafe {
                sensors_sys::sensors_get_detected_chips(
                    0 as usize as *const _,
                    &mut number as *mut _,
                )
            };

            if chip.is_null() {
                break;
            }

            result.push(Chip {
                prefix: unsafe { CStr::from_ptr((*chip).prefix) }
                    .to_str()?
                    .to_string(),
                bus: Bus {
                    r#type: BusType::from_sensors_bus_type(unsafe { *chip }.bus.type_)
                        .ok_or(Error::BusType)?,
                    number: unsafe { *chip }.bus.nr,
                },
                address: unsafe { *chip }.addr,
                path: unsafe { CStr::from_ptr((*chip).path) }
                    .to_str()?
                    .to_string()
                    .into(),
                handle: unsafe { &*chip },
            });
        }

        Ok(result)
    }
}

impl Drop for Sensors {
    fn drop(&mut self) {
        unsafe {
            sensors_sys::sensors_cleanup();
        }
    }
}

#[derive(Debug, Clone)]
pub struct Chip<'a> {
    prefix: String,
    bus: Bus,
    address: i32,
    path: PathBuf,
    handle: &'a sensors_sys::sensors_chip_name,
}

impl<'a> Chip<'a> {
    pub fn prefix(&self) -> &str {
        self.prefix.as_str()
    }

    pub fn bus(&self) -> Bus {
        self.bus
    }

    pub fn address(&self) -> i32 {
        self.address
    }

    pub fn path(&self) -> &Path {
        self.path.as_path()
    }

    pub fn features(&self) -> Result<Vec<Feature>, Error> {
        let mut result = Vec::new();

        let mut number = 0;
        loop {
            let feature =
                unsafe { sensors_sys::sensors_get_features(self.handle, &mut number as *mut _) };

            if feature.is_null() {
                break;
            }

            result.push(Feature {
                name: unsafe { CStr::from_ptr((*feature).name) }
                    .to_str()?
                    .to_string(),
                number: unsafe { *feature }.number,
                r#type: FeatureType::from_sensor_feature_type(unsafe { *feature }.type_),
                first_subfeature: unsafe { *feature }.first_subfeature,
                chip: self.handle,
                handle: unsafe { &*feature },
            });
        }

        Ok(result)
    }

    pub fn adapter_name(&self) -> Result<String, Error> {
        let sensor_name = unsafe { sensors_sys::sensors_get_adapter_name(&self.handle.bus) };

        if !sensor_name.is_null() {
            Ok(unsafe { CStr::from_ptr(sensor_name) }.to_str()?.to_string())
        } else {
            Err(Error::Unknown)
        }
    }

    // int sensors_do_chip_sets(const sensors_chip_name *name);
}

#[derive(Debug, PartialEq, Copy, Clone)]
#[repr(i32)]
pub enum BusType {
    Any = sensors_sys::SENSORS_BUS_TYPE_ANY,
    I2C = sensors_sys::SENSORS_BUS_TYPE_I2C as i32,
    ISA = sensors_sys::SENSORS_BUS_TYPE_ISA as i32,
    PCI = sensors_sys::SENSORS_BUS_TYPE_PCI as i32,
    SPI = sensors_sys::SENSORS_BUS_TYPE_SPI as i32,
    Virtual = sensors_sys::SENSORS_BUS_TYPE_VIRTUAL as i32,
    ACPI = sensors_sys::SENSORS_BUS_TYPE_ACPI as i32,
    HID = sensors_sys::SENSORS_BUS_TYPE_HID as i32,
    MDIO = sensors_sys::SENSORS_BUS_TYPE_MDIO as i32,
    SCSI = sensors_sys::SENSORS_BUS_TYPE_SCSI as i32,
}

impl BusType {
    pub fn from_sensors_bus_type(bus_type: i16) -> Option<Self> {
        match bus_type as i32 {
            sensors_sys::SENSORS_BUS_TYPE_ANY => Some(Self::Any),
            _ => match bus_type as u32 {
                sensors_sys::SENSORS_BUS_TYPE_I2C => Some(Self::I2C),
                sensors_sys::SENSORS_BUS_TYPE_ISA => Some(Self::ISA),
                sensors_sys::SENSORS_BUS_TYPE_PCI => Some(Self::PCI),
                sensors_sys::SENSORS_BUS_TYPE_SPI => Some(Self::SPI),
                sensors_sys::SENSORS_BUS_TYPE_VIRTUAL => Some(Self::Virtual),
                sensors_sys::SENSORS_BUS_TYPE_ACPI => Some(Self::ACPI),
                sensors_sys::SENSORS_BUS_TYPE_HID => Some(Self::HID),
                sensors_sys::SENSORS_BUS_TYPE_MDIO => Some(Self::MDIO),
                sensors_sys::SENSORS_BUS_TYPE_SCSI => Some(Self::SCSI),
                _ => None,
            },
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Bus {
    pub r#type: BusType,
    pub number: i16,
}

#[derive(Debug, PartialEq, Copy, Clone)]
#[repr(u32)]
pub enum FeatureType {
    In = sensors_sys::sensors_feature_type_SENSORS_FEATURE_IN,
    Fan = sensors_sys::sensors_feature_type_SENSORS_FEATURE_FAN,
    Temperature = sensors_sys::sensors_feature_type_SENSORS_FEATURE_TEMP,
    Power = sensors_sys::sensors_feature_type_SENSORS_FEATURE_POWER,
    Energy = sensors_sys::sensors_feature_type_SENSORS_FEATURE_ENERGY,
    Current = sensors_sys::sensors_feature_type_SENSORS_FEATURE_CURR,
    Humidity = sensors_sys::sensors_feature_type_SENSORS_FEATURE_HUMIDITY,
    MaximumMain = sensors_sys::sensors_feature_type_SENSORS_FEATURE_MAX_MAIN,
    Vid = sensors_sys::sensors_feature_type_SENSORS_FEATURE_VID,
    Intrusion = sensors_sys::sensors_feature_type_SENSORS_FEATURE_INTRUSION,
    MaximumOther = sensors_sys::sensors_feature_type_SENSORS_FEATURE_MAX_OTHER,
    BeepEnable = sensors_sys::sensors_feature_type_SENSORS_FEATURE_BEEP_ENABLE,
    Maximum = sensors_sys::sensors_feature_type_SENSORS_FEATURE_MAX,
    Unknown = sensors_sys::sensors_feature_type_SENSORS_FEATURE_UNKNOWN,
}

impl FeatureType {
    pub fn from_sensor_feature_type(sensors_feature_type: u32) -> Self {
        match sensors_feature_type {
            sensors_sys::sensors_feature_type_SENSORS_FEATURE_IN => Self::In,
            sensors_sys::sensors_feature_type_SENSORS_FEATURE_FAN => Self::Fan,
            sensors_sys::sensors_feature_type_SENSORS_FEATURE_TEMP => Self::Temperature,
            sensors_sys::sensors_feature_type_SENSORS_FEATURE_POWER => Self::Power,
            sensors_sys::sensors_feature_type_SENSORS_FEATURE_ENERGY => Self::Energy,
            sensors_sys::sensors_feature_type_SENSORS_FEATURE_CURR => Self::Current,
            sensors_sys::sensors_feature_type_SENSORS_FEATURE_HUMIDITY => Self::Humidity,
            sensors_sys::sensors_feature_type_SENSORS_FEATURE_MAX_MAIN => Self::MaximumMain,
            sensors_sys::sensors_feature_type_SENSORS_FEATURE_VID => Self::Vid,
            sensors_sys::sensors_feature_type_SENSORS_FEATURE_INTRUSION => Self::Intrusion,
            sensors_sys::sensors_feature_type_SENSORS_FEATURE_MAX_OTHER => Self::MaximumOther,
            sensors_sys::sensors_feature_type_SENSORS_FEATURE_BEEP_ENABLE => Self::BeepEnable,
            sensors_sys::sensors_feature_type_SENSORS_FEATURE_MAX => Self::Maximum,
            _ => Self::Unknown,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Feature<'a> {
    name: String,
    number: i32,
    r#type: FeatureType,
    first_subfeature: i32,
    chip: &'a sensors_sys::sensors_chip_name,
    handle: &'a sensors_sys::sensors_feature,
}

impl<'a> Feature<'a> {
    pub fn label(&self) -> Result<String, Error> {
        let label = unsafe { sensors_sys::sensors_get_label(self.chip, self.handle) };

        if !label.is_null() {
            Ok(unsafe { CString::from_raw(label) }.to_str()?.to_string())
        } else {
            Err(Error::Unknown)
        }
    }

    pub fn r#type(&self) -> FeatureType {
        self.r#type
    }

    pub fn all_subfeatures(&self) -> Result<Vec<SubFeature>, Error> {
        let mut result = Vec::new();

        let mut number = 0;
        loop {
            let sub_feature = unsafe {
                sensors_sys::sensors_get_all_subfeatures(
                    self.chip,
                    self.handle,
                    &mut number as *mut _,
                )
            };

            if sub_feature.is_null() {
                break;
            }

            result.push(SubFeature {
                name: unsafe { CStr::from_ptr((*sub_feature).name) }
                    .to_str()?
                    .to_string(),
                number: unsafe { *sub_feature }.number,
                r#type: SubFeatureType::from_sensor_subfeature_type(unsafe { *sub_feature }.type_),
                flags: SensorFlags {
                    bits: unsafe { *sub_feature }.flags,
                },
                chip: self.chip,
                handle: unsafe { &*sub_feature },
            });
        }

        Ok(result)
    }

    pub fn subfeature(&self, r#type: SubFeatureType) -> Result<SubFeature, Error> {
        let subfeature =
            unsafe { sensors_sys::sensors_get_subfeature(self.chip, self.handle, r#type as u32) };

        if !subfeature.is_null() {
            let subfeature = unsafe { &*subfeature };
            Ok(SubFeature {
                name: unsafe { CStr::from_ptr(subfeature.name) }
                    .to_str()?
                    .to_string(),
                number: subfeature.number,
                r#type,
                flags: SensorFlags {
                    bits: subfeature.flags,
                },
                chip: self.chip,
                handle: subfeature,
            })
        } else {
            Err(Error::NoSuchFeature)
        }
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
#[repr(u32)]
pub enum SubFeatureType {
    InInput = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_INPUT,
    InMinimum = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_MIN,
    InMaximum = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_MAX,
    InLowCritical = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_LCRIT,
    InCritical = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_CRIT,
    InAverage = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_AVERAGE,
    InLowest = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_LOWEST,
    InHighest = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_HIGHEST,
    InAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_ALARM,
    InMinimumAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_MIN_ALARM,
    InMaximumAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_MAX_ALARM,
    InBeep = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_BEEP,
    InLowCriticalAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_LCRIT_ALARM,
    InCriticalAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_CRIT_ALARM,
    FanInput = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_INPUT,
    FanMinimum = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_MIN,
    FanMaximum = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_MAX,
    FanAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_ALARM,
    FanFault = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_FAULT,
    FanDIV = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_DIV,
    FanBeep = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_BEEP,
    FanPulses = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_PULSES,
    FanMinimumAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_MIN_ALARM,
    FanMaximumAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_MAX_ALARM,
    TemperatureInput = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_INPUT,
    TemperatureMaximum = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_MAX,
    TemperatureMaximumHysteresis =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_MAX_HYST,
    TemperatureMinimum = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_MIN,
    TemperatureCritical = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_CRIT,
    TemperatureCriticalHysteresis =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_CRIT_HYST,
    TemperatureLowCritical = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_LCRIT,
    TemperatureEmergency = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_EMERGENCY,
    TemperatureEmergencyHysteresis =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_EMERGENCY_HYST,
    TemperatureLowest = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_LOWEST,
    TemperatureHighest = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_HIGHEST,
    TemperatureMinimumHysteresis =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_MIN_HYST,
    TemperatureLowCriticalHysteresis =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_LCRIT_HYST,
    TemperatureAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_ALARM,
    TemperatureMaximumAlarm =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_MAX_ALARM,
    TemperatureMinimumAlarm =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_MIN_ALARM,
    TemperatureCriticalAlarm =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_CRIT_ALARM,
    TemperatureFault = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_FAULT,
    TemperatureType = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_TYPE,
    TemperatureOffset = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_OFFSET,
    TemperatureBeep = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_BEEP,
    TemperatureEmergencyAlarm =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_EMERGENCY_ALARM,
    TemperatureLowCriticalAlarm =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_LCRIT_ALARM,
    PowerAverage = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_AVERAGE,
    PowerAverageHighest =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_AVERAGE_HIGHEST,
    PowerAverageLowest =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_AVERAGE_LOWEST,
    PowerInput = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_INPUT,
    PowerInputHighest = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_INPUT_HIGHEST,
    PowerInputLowest = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_INPUT_LOWEST,
    PowerCapacity = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_CAP,
    PowerCapacityHysteresis =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_CAP_HYST,
    PowerMaximum = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_MAX,
    PowerCritical = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_CRIT,
    PowerMinimum = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_MIN,
    PowerLowCritical = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_LCRIT,
    PowerAverageInterval =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_AVERAGE_INTERVAL,
    PowerAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_ALARM,
    PowerCapacityAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_CAP_ALARM,
    PowerMaximumAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_MAX_ALARM,
    PowerCriticalAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_CRIT_ALARM,
    PowerMinimumAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_MIN_ALARM,
    PowerLowCriticalAlarm =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_LCRIT_ALARM,
    EnergyInput = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_ENERGY_INPUT,
    CurrentInput = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_INPUT,
    CurrentMinimum = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_MIN,
    CurrentMaximum = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_MAX,
    CurrentLowCritical = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_LCRIT,
    CurrentCritical = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_CRIT,
    CurrentAverage = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_AVERAGE,
    CurrentLowest = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_LOWEST,
    CurrentHighest = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_HIGHEST,
    CurrentAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_ALARM,
    CurrentMinimumAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_MIN_ALARM,
    CurrentMaximumAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_MAX_ALARM,
    CurrentBeep = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_BEEP,
    CurrentLowCriticalAlarm =
        sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_LCRIT_ALARM,
    CurrentCriticalAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_CRIT_ALARM,
    HumidityInput = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_HUMIDITY_INPUT,
    VID = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_VID,
    IntrusionAlarm = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_INTRUSION_ALARM,
    IntrusionBeep = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_INTRUSION_BEEP,
    BeepEnable = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_BEEP_ENABLE,
    Unknown = sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_UNKNOWN,
}

impl SubFeatureType {
    pub fn from_sensor_subfeature_type(sensors_subfeature_type: u32) -> Self {
        match sensors_subfeature_type {
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_INPUT => Self::InInput,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_MIN => Self::InMinimum,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_MAX => Self::InMaximum,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_LCRIT => Self::InLowCritical,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_CRIT => Self::InCritical,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_AVERAGE => Self::InAverage,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_LOWEST => Self::InLowest,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_HIGHEST => Self::InHighest,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_ALARM => Self::InAlarm,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_MIN_ALARM => {
                Self::InMinimumAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_MAX_ALARM => {
                Self::InMaximumAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_BEEP => Self::InBeep,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_LCRIT_ALARM => {
                Self::InLowCriticalAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_IN_CRIT_ALARM => {
                Self::InCriticalAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_INPUT => Self::FanInput,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_MIN => Self::FanMinimum,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_MAX => Self::FanMaximum,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_ALARM => Self::FanAlarm,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_FAULT => Self::FanFault,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_DIV => Self::FanDIV,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_BEEP => Self::FanBeep,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_PULSES => Self::FanPulses,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_MIN_ALARM => {
                Self::FanMinimumAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_FAN_MAX_ALARM => {
                Self::FanMaximumAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_INPUT => {
                Self::TemperatureInput
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_MAX => {
                Self::TemperatureMaximum
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_MAX_HYST => {
                Self::TemperatureMaximumHysteresis
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_MIN => {
                Self::TemperatureMinimum
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_CRIT => {
                Self::TemperatureCritical
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_CRIT_HYST => {
                Self::TemperatureCriticalHysteresis
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_LCRIT => {
                Self::TemperatureLowCritical
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_EMERGENCY => {
                Self::TemperatureEmergency
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_EMERGENCY_HYST => {
                Self::TemperatureEmergencyHysteresis
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_LOWEST => {
                Self::TemperatureLowest
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_HIGHEST => {
                Self::TemperatureHighest
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_MIN_HYST => {
                Self::TemperatureMinimumHysteresis
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_LCRIT_HYST => {
                Self::TemperatureLowCriticalHysteresis
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_ALARM => {
                Self::TemperatureAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_MAX_ALARM => {
                Self::TemperatureMaximumAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_MIN_ALARM => {
                Self::TemperatureMinimumAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_CRIT_ALARM => {
                Self::TemperatureCriticalAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_FAULT => {
                Self::TemperatureFault
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_TYPE => {
                Self::TemperatureType
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_OFFSET => {
                Self::TemperatureOffset
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_BEEP => {
                Self::TemperatureBeep
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_EMERGENCY_ALARM => {
                Self::TemperatureEmergencyAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_TEMP_LCRIT_ALARM => {
                Self::TemperatureLowCriticalAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_AVERAGE => {
                Self::PowerAverage
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_AVERAGE_HIGHEST => {
                Self::PowerAverageHighest
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_AVERAGE_LOWEST => {
                Self::PowerAverageLowest
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_INPUT => Self::PowerInput,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_INPUT_HIGHEST => {
                Self::PowerInputHighest
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_INPUT_LOWEST => {
                Self::PowerInputLowest
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_CAP => {
                Self::PowerCapacity
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_CAP_HYST => {
                Self::PowerCapacityHysteresis
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_MAX => Self::PowerMaximum,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_CRIT => {
                Self::PowerCritical
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_MIN => Self::PowerMinimum,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_LCRIT => {
                Self::PowerLowCritical
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_AVERAGE_INTERVAL => {
                Self::PowerAverageInterval
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_ALARM => Self::PowerAlarm,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_CAP_ALARM => {
                Self::PowerCapacityAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_MAX_ALARM => {
                Self::PowerMaximumAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_CRIT_ALARM => {
                Self::PowerCriticalAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_MIN_ALARM => {
                Self::PowerMinimumAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_POWER_LCRIT_ALARM => {
                Self::PowerLowCriticalAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_ENERGY_INPUT => {
                Self::EnergyInput
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_INPUT => {
                Self::CurrentInput
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_MIN => {
                Self::CurrentMinimum
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_MAX => {
                Self::CurrentMaximum
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_LCRIT => {
                Self::CurrentLowCritical
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_CRIT => {
                Self::CurrentCritical
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_AVERAGE => {
                Self::CurrentAverage
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_LOWEST => {
                Self::CurrentLowest
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_HIGHEST => {
                Self::CurrentHighest
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_ALARM => {
                Self::CurrentAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_MIN_ALARM => {
                Self::CurrentMinimumAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_MAX_ALARM => {
                Self::CurrentMaximumAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_BEEP => Self::CurrentBeep,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_LCRIT_ALARM => {
                Self::CurrentLowCriticalAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_CURR_CRIT_ALARM => {
                Self::CurrentCriticalAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_HUMIDITY_INPUT => {
                Self::HumidityInput
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_VID => Self::VID,
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_INTRUSION_ALARM => {
                Self::IntrusionAlarm
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_INTRUSION_BEEP => {
                Self::IntrusionBeep
            }
            sensors_sys::sensors_subfeature_type_SENSORS_SUBFEATURE_BEEP_ENABLE => Self::BeepEnable,
            _ => Self::Unknown,
        }
    }
}

#[derive(Debug, Clone)]
pub struct SubFeature<'a> {
    name: String,
    number: i32,
    r#type: SubFeatureType,
    flags: SensorFlags,
    chip: &'a sensors_sys::sensors_chip_name,
    handle: &'a sensors_sys::sensors_subfeature,
}

impl<'a> SubFeature<'a> {
    pub fn get_value(&self) -> Result<f64, Error> {
        let mut value = 0.0;
        let error = unsafe { sensors_sys::sensors_get_value(self.chip, self.number, &mut value) };

        if error != 0 {
            Err(Error::from_sensors_error_number(error))
        } else {
            Ok(value)
        }
    }
    // int sensors_set_value(const sensors_chip_name *name, int subfeat_nr, double value);
}
