use crate::systeminfo::udisks2::client::Client;
use lazy_static::lazy_static;
use serde::Serialize;

const SECTOR_SIZE: f32 = 512f32;
const SNAPSHOT_TIME_DELTA_MS: f32 = 1000f32;

lazy_static! {
    static ref UDISKS2_CLIENT: Client = {
        let client = Client::new();
        if client.is_ok() {
            return client.unwrap();
        }

        panic!("Failed to initialize UDisks2 client!");
    };
}

#[derive(Clone, Debug, Serialize)]
pub struct Filesystem {
    pub mount_points: Vec<String>,
    pub filesystem: String,
    pub size_bytes: u64,
    pub used_bytes: u64,
}

#[derive(Debug, Serialize)]
pub struct BlockDevice {
    pub device_file: String,
    pub size: u64,
    pub label: String,
    pub uuid: String,
    pub filesystem: Option<Filesystem>,
    pub read_speed_bps: f32,
    pub write_speed_bps: f32,
}

#[derive(Debug, Serialize)]
pub struct Drive {
    pub is_physical_drive: bool,
    pub model: String,
    pub serial_no: String,
    pub capacity: u64,
    pub block_devices: Vec<BlockDevice>,
    pub has_smart_info: bool,
    pub temperature_celsius: f64,
    pub smart_status: String,
}

struct BlockIOStat {
    pub device_name: String,
    pub sectors_read: u64,
    pub sectors_written: u64,
}

pub fn gather() -> Result<Vec<Drive>, String> {
    use std::collections::HashMap;
    use std::thread::sleep;
    use std::time::Duration;

    let udisks2_client = &UDISKS2_CLIENT;

    let mut drive_objects = HashMap::new();

    let block_device_object_paths = udisks2_client.block_device_object_paths()?;
    for object_path in block_device_object_paths {
        let object = udisks2_client.make_object(object_path)?;

        let block = object.as_block();
        if block.is_none() {
            continue;
        }

        let block = block.unwrap();
        let drive = udisks2_client.make_drive(&block);

        let entry = drive_objects.get_mut(&drive);
        if entry.is_none() {
            let mut block_devices = Vec::new();
            block_devices.push((block, object));
            drive_objects.insert(drive, block_devices);
        } else {
            let entry = entry.unwrap();
            entry.push((block, object));
        }
    }

    let mut drives = Vec::new();

    for (drive_object, block_devices) in drive_objects {
        use crate::systeminfo::utilities::filesystem_usage;

        let mut drive = Drive {
            is_physical_drive: false,
            model: "".to_string(),
            serial_no: "".to_string(),
            capacity: 0,
            block_devices: vec![],
            has_smart_info: false,
            temperature_celsius: -1_f64,
            smart_status: "".to_string(),
        };

        for (block, object) in block_devices {
            let block_filesystem = object.as_filesystem();
            let filesystem = if block_filesystem.is_some() {
                let block_filesystem = block_filesystem.unwrap();

                let mount_points = block_filesystem.mount_points();
                let size_bytes = block_filesystem.size_bytes();
                let used_bytes = if !mount_points.is_empty() && size_bytes > 0 {
                    filesystem_usage(mount_points[0].as_str(), size_bytes)?
                } else {
                    0
                };

                Some(Filesystem {
                    mount_points,
                    filesystem: block.r#type(),
                    size_bytes,
                    used_bytes,
                })
            } else {
                None
            };

            drive.block_devices.push(BlockDevice {
                device_file: block.device_file(),
                size: block.size(),
                label: block.label(),
                uuid: block.uuid(),
                filesystem,
                read_speed_bps: 0.0,
                write_speed_bps: 0.0,
            });
        }

        if drive_object.is_some() {
            let drive_object = drive_object.unwrap();
            drive.is_physical_drive = true;
            drive.model = drive_object.model();
            drive.serial_no = drive_object.serial();
            drive.capacity = drive_object.capacity();
            drive.has_smart_info = drive_object.has_smart_info();
            drive.temperature_celsius = drive_object.temperature_celsius();
            drive.smart_status = drive_object.selftest_status();
        }

        drive
            .block_devices
            .sort_unstable_by(|block_device1, block_device2| {
                block_device1.device_file.cmp(&block_device2.device_file)
            });
        drives.push(drive);
    }

    let snapshot1 = read_io_stats()?;
    sleep(Duration::from_millis(SNAPSHOT_TIME_DELTA_MS as u64));
    let snapshot2 = read_io_stats()?;

    update_rw_speeds(&snapshot1, &snapshot2, &mut drives)?;

    Ok(drives)
}

fn read_io_stats() -> Result<Vec<BlockIOStat>, String> {
    use std::fs::OpenOptions;
    use std::io::{BufRead, BufReader};

    let mut result = Vec::new();

    let file = OpenOptions::new()
        .read(true)
        .open("/proc/diskstats")
        .map_err(|e| e.to_string())?;
    let mut reader = BufReader::new(file);
    loop {
        let mut line = String::new();
        let size = reader.read_line(&mut line).map_err(|e| e.to_string())?;

        if size == 0 {
            break;
        }

        if line.is_empty() {
            continue;
        }

        const INDEX_NAME: usize = 2;
        const INDEX_SECTORS_READ: usize = 5;
        const INDEX_SECTORS_WRITTEN: usize = 9;

        let mut io_info = BlockIOStat {
            device_name: "".to_string(),
            sectors_read: 0,
            sectors_written: 0,
        };

        for (index, field) in line.split_whitespace().enumerate() {
            match index {
                INDEX_NAME => {
                    io_info.device_name = field.trim().to_string();
                }
                INDEX_SECTORS_READ => {
                    let sectors = field.trim().parse::<u64>().map_err(|e| e.to_string())?;
                    io_info.sectors_read = sectors;
                }
                INDEX_SECTORS_WRITTEN => {
                    let sectors = field.trim().parse::<u64>().map_err(|e| e.to_string())?;
                    io_info.sectors_written = sectors;

                    result.push(io_info);
                    break;
                }
                _ => {}
            }
        }
    }

    Ok(result)
}

fn update_rw_speeds(
    snapshot1: &Vec<BlockIOStat>,
    snapshot2: &Vec<BlockIOStat>,
    drives: &mut Vec<Drive>,
) -> Result<(), String> {
    if snapshot1.len() != snapshot2.len() {
        Err("Snapshots are incompatible")?;
    }

    let entry_count = snapshot1.len();

    for i in 0..entry_count {
        let disk_data1 = &snapshot1[i];
        let disk_data2 = &snapshot2[i];

        if disk_data1.device_name != disk_data2.device_name {
            Err("Snapshots are in different order")?;
        }

        // In case of overflow
        if snapshot1[i].sectors_read > snapshot2[i].sectors_read {
            continue;
        }

        if snapshot1[i].sectors_written > snapshot2[i].sectors_written {
            continue;
        }

        let read_speed = (disk_data2.sectors_read - disk_data1.sectors_read) as f32
            / (SNAPSHOT_TIME_DELTA_MS / 1000.0);
        let write_speed = (disk_data2.sectors_written - disk_data1.sectors_written) as f32
            / (SNAPSHOT_TIME_DELTA_MS / 1000.0);

        for drive in drives.iter_mut() {
            let mut found = false;
            for block_device in &mut drive.block_devices {
                if block_device.device_file.split('/').last().unwrap() == disk_data1.device_name {
                    block_device.read_speed_bps = read_speed * SECTOR_SIZE;
                    block_device.write_speed_bps = write_speed * SECTOR_SIZE;

                    found = true;
                    break;
                }
            }

            if found {
                break;
            }
        }
    }

    Ok(())
}
