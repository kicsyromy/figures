mod service;
mod systeminfo;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let _ = service::run([0, 0, 0, 0], 3000).await;

    Ok(())
}
